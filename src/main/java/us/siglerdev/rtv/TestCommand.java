package us.siglerdev.rtv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import net.minecraft.server.v1_7_R4.PacketPlayOutWorldParticles;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R4.CraftWorld;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import us.siglerdev.rtv.manager.team.GameTeam;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.manager.StatsManager;
import us.siglerdev.rtv.manager.VillagerManager;
import us.siglerdev.rtv.resource.Config;
import us.siglerdev.rtv.resource.CustomItem;
import us.siglerdev.rtv.resource.GameItems;
import us.siglerdev.rtv.resource.ParticleEffect;
import us.siglerdev.rtv.resource.ParticleEffect.ParticleType;
import us.siglerdev.rtv.util.Util;

import java.util.ArrayList;
import java.util.List;

public class TestCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(args.length == 0) {
			sender.sendMessage("Get more args");
			return false;
		} else {
			Player player = (Player) sender;
			switch(args[0]){
			case "villager":
				VillagerManager v = new VillagerManager(((CraftWorld) player.getWorld()).getHandle());
				v.setPosition(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ());
				((CraftWorld)player.getWorld()).getHandle().addEntity(v);
				break;
			case "kit":
				Util.showClassSelector(player, "Select Kit");
				break;
			case "l":
				Bukkit.broadcastMessage("LOC: " + player.getLocation());
				break;
			case "team":
				PlayerMeta.getMeta(player).setTeam(GameTeam.valueOf(args[1].toUpperCase()));
				player.sendMessage(ChatColor.RED + "Your new team is: " + PlayerMeta.getMeta(player).getTeam());
				player.setPlayerListName(PlayerMeta.getMeta(player).getTeam().color() + player.getName());
				break;
			case "remove":
				for (Entity e : player.getNearbyEntities(50, 50, 50))
					if (e instanceof LivingEntity && !(e instanceof Player))
						e.remove();
				break;
			case "win":
				RescueTheVillager.get().triggerWin(GameTeam.BLUE, player);
				break;
			case "start":
				Util.checkStarting(true);
				player.sendMessage(ChatColor.GREEN + "The game has started!");
				break;
			case "end":
				RescueTheVillager.get().endGame();
				player.sendMessage(ChatColor.RED + "The game has ended!");
				break;
			case "items":
				player.getInventory().addItem(GameItems.KIT_SELECTOR);
				player.getInventory().addItem(GameItems.MAGIC_KEY);
				player.getInventory().addItem(GameItems.TEAM_CHOOSER);
				player.getInventory().addItem(GameItems.TEAM_CHOOSER_BLUE);
				player.getInventory().addItem(GameItems.TEAM_CHOOSER_WHITE);
				break;
			}
			return false;
		}
	}
}
