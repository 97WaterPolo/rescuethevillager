package us.siglerdev.rtv.util;

import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryType;

public class InventoryUtil {

	public static boolean isPlace(InventoryAction inventoryAction) {
		switch(inventoryAction) {
			case PLACE_ALL:
			case PLACE_ONE:
			case PLACE_SOME:
				return true;
			default:
				return false;
		}
	}

	public static boolean isInventory(InventoryType inventoryType) {
		switch(inventoryType) {
			case ANVIL:
			case BEACON:
			case BREWING:
			case CHEST:
			case CRAFTING:
			case DISPENSER:
			case DROPPER:
			case ENCHANTING:
			case ENDER_CHEST:
			case FURNACE:
			case HOPPER:
			case MERCHANT:
			case WORKBENCH:
				return true;
			default:
				return false;
		}
	}
}