package us.siglerdev.rtv.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import net.minecraft.server.v1_7_R4.EntityHuman;
import net.minecraft.server.v1_7_R4.EntityLiving;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.craftbukkit.v1_7_R4.CraftWorld;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftAnimals;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftMonster;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftSkeleton;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.Potion.Tier;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.bukkit.util.Vector;

import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.event.SoulboundListener;
import us.siglerdev.rtv.manager.team.GameTeam;
import us.siglerdev.rtv.manager.Kit;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.manager.SiglerBoard;
import us.siglerdev.rtv.manager.StatsManager;
import us.siglerdev.rtv.manager.VillagerKitManager;
import us.siglerdev.rtv.resource.Config;
import us.siglerdev.rtv.resource.CustomItem;
import us.siglerdev.rtv.resource.GameItems;
import us.siglerdev.rtv.resource.ParticleEffect;
import us.siglerdev.rtv.resource.ParticleEffect.ParticleType;

import java.util.ArrayList;
import java.util.List;

public class Util {

	public List<BukkitTask> btl = new ArrayList<BukkitTask>();
	public static void resetPlayer(Player p){
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);
		p.getInventory().addItem(SoulboundListener.makeSoulbound2(GameItems.TEAM_CHOOSER));
		p.getInventory().addItem(SoulboundListener.makeSoulbound2(GameItems.KIT_SELECTOR));
		p.setExp(0);
		p.setLevel(0);
		p.setMaxHealth(20);
		p.setHealth(20);
		p.setFoodLevel(20);
		p.setSaturation(20);
		updateLobbyTab(p);
	}
	public static void updateLobbyTab(Player player){
		if (!RescueTheVillager.get().gameStarted){
			String name = ChatColor.GRAY.toString() + player.getName();
			if (name.length() > 16)
				player.setPlayerListName(name.substring(0,16));
			else
				player.setPlayerListName(name);
		}
	}
	public static boolean cDown = false;
	public static void checkStarting(boolean b){
		if (cDown)
			return;
		if (Bukkit.getOnlinePlayers().size() >= Config.get().getConfig().getInt("General.MinPlayers") && (RescueTheVillager.get().gameStarted == false) || b){
			cDown = true;
			new BukkitRunnable(){
				int counter = 20;
				@Override
				public void run(){
					if (counter == 3){
						Location wSpawn = Util.parseLocation(Config.get().getMap().getString("World"), Config.get().getMap().getString("WhiteSpawn"));
						Location bSpawn = Util.parseLocation(Config.get().getMap().getString("World"), Config.get().getMap().getString("BlueSpawn"));
						wSpawn.getChunk().load();
						bSpawn.getChunk().load();
					}
					if (counter <= 0){
						Bukkit.broadcastMessage(ChatColor.GREEN + "The Game has started!");
						RescueTheVillager.get().start();
						RescueTheVillager.get().gameStarted = true;
						this.cancel();
					}else{
						for (Player p : Bukkit.getOnlinePlayers())
							p.playSound(p.getLocation(), Sound.CLICK, 1F, 1F);
						Bukkit.broadcastMessage(ChatColor.GOLD + "Game Starting in " + counter);
					}
					counter--;
				}
			}.runTaskTimer(RescueTheVillager.get(), 20, 20);
		}
	}
	public static void updateGameScoreboard(Player player) {
		StatsManager s = RescueTheVillager.get().getStats();
		SiglerBoard sb = RescueTheVillager.get().getSiglerBoard();
		sb.setScore(ChatColor.translateAlternateColorCodes('&', "&6&lKills:"), s.getKills(player));
		sb.setScore(ChatColor.translateAlternateColorCodes('&', "&6&lDeaths:"), s.getDeaths(player));
		sb.setScoreboard(player);
	}
	public static void forceTabUpdate(Player player){
		String name = PlayerMeta.getMeta(player).getTeam().color().toString() + player.getName();
		if (name.length() > 16)
			player.setPlayerListName(name.substring(0,16));
		else
			player.setPlayerListName(name);
	}
	public static List<Player> nearbyPlayers(Player p) {
		List<Player> l = new ArrayList<>();
		for(Entity e : p.getNearbyEntities(16, 16, 16)) {
			if(e instanceof Player) {
				l.add((Player) e);
			}
		}
		return l;
	}
	public static Location parseLocation(World w, String in) {
		String[] params = in.split(",");
		for(String s : params)
			s.replace("-0", "0");
		if(params.length == 3 || params.length == 5) {
			double x = Double.parseDouble(params[0]);
			double y = Double.parseDouble(params[1]);
			double z = Double.parseDouble(params[2]);
			Location loc = new Location(w, x, y, z);
			if(params.length == 5) {
				loc.setYaw(Float.parseFloat(params[3]));
				loc.setPitch(Float.parseFloat(params[4]));
			}
			return loc;
		}
		return null;
	}
	public static void generateTeams(){
		for (Player p : Bukkit.getOnlinePlayers()){
			if (PlayerMeta.getMeta(p).getTeam() == GameTeam.NONE){
				PlayerMeta.getMeta(p).setTeam(getSmalletTeam());
			}
		}
		RescueTheVillager.get().getSiglerBoard().updateTeams();
	}
	@SuppressWarnings("deprecation")
	public static ItemStack[] getRandomItems(){
		Random r = new Random();
		int size = 2+r.nextInt(2);
		Material[] randItems = new Material[]{
				Material.IRON_HELMET,Material.IRON_HELMET, Material.IRON_CHESTPLATE, Material.IRON_CHESTPLATE, Material.IRON_LEGGINGS, Material.IRON_LEGGINGS,
				Material.IRON_BOOTS, Material.IRON_BOOTS,Material.IRON_SWORD,Material.IRON_SWORD, Material.CHAINMAIL_HELMET, Material.CHAINMAIL_HELMET, Material.CHAINMAIL_HELMET,
				Material.CHAINMAIL_CHESTPLATE, Material.CHAINMAIL_CHESTPLATE, Material.CHAINMAIL_CHESTPLATE, Material.CHAINMAIL_LEGGINGS, Material.CHAINMAIL_LEGGINGS,Material.CHAINMAIL_LEGGINGS,
				Material.CHAINMAIL_BOOTS,Material.CHAINMAIL_BOOTS,Material.CHAINMAIL_BOOTS,Material.IRON_SWORD,Material.IRON_SWORD,Material.IRON_SWORD,Material.IRON_SWORD,Material.IRON_SWORD,
				Material.STONE_SWORD,Material.STONE_SWORD,Material.STONE_SWORD,Material.STONE_SWORD,Material.STONE_SWORD,Material.STONE_SWORD,Material.STONE_SWORD,Material.STONE_SWORD,
				Material.DIAMOND_SWORD, Material.DIAMOND_SWORD, Material.GOLD_HELMET, Material.GOLD_HELMET, Material.GOLD_HELMET, Material.GOLD_HELMET, Material.GOLD_HELMET, Material.GOLD_HELMET,
				Material.GOLD_CHESTPLATE,Material.GOLD_CHESTPLATE,Material.GOLD_CHESTPLATE,Material.GOLD_CHESTPLATE,Material.GOLD_CHESTPLATE,Material.GOLD_CHESTPLATE,
				Material.GOLD_LEGGINGS,Material.GOLD_LEGGINGS,Material.GOLD_LEGGINGS,Material.GOLD_CHESTPLATE,Material.GOLD_LEGGINGS,Material.GOLD_LEGGINGS,
				Material.GOLD_BOOTS,Material.GOLD_BOOTS,Material.GOLD_BOOTS,Material.GOLD_BOOTS,Material.GOLD_BOOTS,Material.GOLD_BOOTS,Material.GOLD_BOOTS,Material.ENDER_PEARL};
		ItemStack[] cI = new ItemStack[]{
				new Potion(PotionType.INSTANT_HEAL, Tier.TWO).toItemStack(1),new Potion(PotionType.INSTANT_HEAL, Tier.TWO).toItemStack(1),new Potion(PotionType.INSTANT_HEAL, Tier.TWO).toItemStack(1),
				new Potion(PotionType.INSTANT_HEAL, Tier.ONE).toItemStack(1),new Potion(PotionType.INSTANT_HEAL, Tier.ONE).toItemStack(1),new Potion(PotionType.INSTANT_HEAL, Tier.ONE).toItemStack(1),
				new Potion(PotionType.INSTANT_HEAL, Tier.ONE,true).toItemStack(1),new Potion(PotionType.INSTANT_HEAL, Tier.ONE,true).toItemStack(1),new Potion(PotionType.INSTANT_HEAL, Tier.ONE,true).toItemStack(1),
				new ItemStack(Material.COOKED_BEEF,r.nextInt(63)),new ItemStack(Material.EGG,r.nextInt(15)),new ItemStack(Material.ARROW, 1+r.nextInt(9)),new ItemStack(Material.ARROW, 1+r.nextInt(9)),
				new ItemStack(Material.ARROW, 1+r.nextInt(9)),new ItemStack(Material.ARROW, 1+r.nextInt(9)),new ItemStack(Material.ARROW, 1+r.nextInt(9)),
				new ItemStack(Material.BOW, 1, (short)384),new ItemStack(Material.BOW, 1, (short)384),new ItemStack(Material.BOW, 1, (short)384),new ItemStack(Material.BOW, 1, (short)384),
		};
		ItemStack[] i = new ItemStack[size];
		for (int x = 0; x <size ; x++){
			int t = r.nextInt(2);
			if (t == 0)
				i[x] = new ItemStack(randItems[r.nextInt(randItems.length)]);
			else 
				i[x] = cI[r.nextInt(cI.length)];
		}
		return i;
	}
	public static Location parseLocation(String w, String in) {
		String[] params = in.split(",");
		for(String s : params)
			s.replace("-0", "0");
		if(params.length == 3 || params.length == 5) {
			double x = Double.parseDouble(params[0]);
			double y = Double.parseDouble(params[1]);
			double z = Double.parseDouble(params[2]);
			Location loc = new Location(Bukkit.getWorld(w), x, y, z);
			if(params.length == 5) {
				loc.setYaw(Float.parseFloat(params[4]));
				loc.setPitch(Float.parseFloat(params[5]));
			}
			return loc;
		}
		return null;
	}
	public static List<Location> circle (Location loc, Integer r, Integer h, Boolean hollow, Boolean sphere, int plus_y) {
		List<Location> circleblocks = new ArrayList<Location>();
		int cx = loc.getBlockX();
		int cy = loc.getBlockY();
		int cz = loc.getBlockZ();
		for (int x = cx - r; x <= cx +r; x++)
			for (int z = cz - r; z <= cz +r; z++)
				for (int y = (sphere ? cy - r : cy); y < (sphere ? cy + r : cy + h); y++) {
					double dist = (cx - x) * (cx - x) + (cz - z) * (cz - z) + (sphere ? (cy - y) * (cy - y) : 0);
					if (dist < r*r && !(hollow && dist < (r-1)*(r-1))) {
						Location l = new Location(loc.getWorld(), x, y + plus_y, z);
						circleblocks.add(l);
					}
				}

		return circleblocks;
	}
	@SuppressWarnings("deprecation")
	public static void gameRewards(GameTeam g){
		for (Player p : Bukkit.getOnlinePlayers()){
			int x = 0;
			if (PlayerMeta.getMeta(p).getTeam() == g)
				x+=25;
			else
				x+=7;
			Player releaser = Bukkit.getPlayer(RescueTheVillager.get().rescuer);
			if (releaser != null)
				if (p.getName().equalsIgnoreCase(releaser.getName()))
					x+=25;
				else
					x+=8;
			int kd = RescueTheVillager.get().getStats().getKills(p) - RescueTheVillager.get().getStats().getDeaths(p);
			x+=6;
			if (kd > 0)
				x+=kd;
			p.sendMessage(ChatColor.GOLD + "You earned a total of " + x + " coins!");
			//TODO Add it to the coin system.
		}
	}
	public static GameTeam getSmalletTeam() {
		GameTeam teamMin = GameTeam.WHITE;
		ArrayList<GameTeam> gt = new ArrayList<>();
		gt.add(GameTeam.WHITE);
		gt.add(GameTeam.BLUE);
		for(GameTeam team : gt) {
			if(team.getPlayers().size() <= teamMin.getPlayers().size()) {
				teamMin = team;
			}
		}
		return teamMin;
	}
	public static boolean carryingVillager(Player player){
		if (player.getPassenger() != null)
				return true;
		return false;
	}
	public static void spawnKitVillagers(){
		World w = Bukkit.getWorld(Config.get().getMap().getString("World"));
		final Location l = Util.parseLocation(w, Config.get().getMap().getString("KitNPC.Blue"));
		final VillagerKitManager v = new VillagerKitManager(((CraftWorld) w).getHandle());
		v.setPositionRotation(l.getX(),l.clone().add(.5,0,.5).getY(),l.getZ(), l.getYaw(),l.getPitch());
		((CraftWorld)w).getHandle().addEntity(v);

		final Location l2 = Util.parseLocation(w, Config.get().getMap().getString("KitNPC.White"));
		final VillagerKitManager v2 = new VillagerKitManager(((CraftWorld) w).getHandle());
		v2.setPositionRotation(l2.getX(),l2.clone().add(.5,0,.5).getY(),l2.getZ(), l2.getYaw(), l2.getPitch());
		((CraftWorld)w).getHandle().addEntity(v2);
	}
	public final static List<BlockState> bsO = new ArrayList<BlockState>();
	public static void genChest(){
		final Random r = new Random();
		final List<BlockState> mod = new ArrayList<BlockState>();
		for (Chunk chunks : Bukkit.getWorld(Config.get().getMap().getString("World")).getLoadedChunks()) {
			for (BlockState chests : chunks.getTileEntities()) {
				if (chests instanceof Chest) {
					((Chest)chests).getBlockInventory().clear();
					bsO.add(chests);
					mod.add(chests);
					chests.getWorld().getBlockAt(chests.getLocation()).setType(Material.AIR);
				}
			}
		}
		/** Generate the chest with the Key **/
		final BlockState key = mod.get(r.nextInt(mod.size()));
		key.getWorld().getBlockAt(key.getLocation()).setType(Material.CHEST);
		Inventory kInv = ((Chest)key.getWorld().getBlockAt(key.getLocation()).getState()).getBlockInventory();
		int[] gA = new int[]{4,9,10,12,14,16,17,22};
		kInv.setItem(13, GameItems.MAGIC_KEY);
		for (int x : gA)
			kInv.setItem(x, new ItemStack(Material.GOLDEN_APPLE, 1, (byte) 1));
		new BukkitRunnable(){
			ParticleEffect p = new ParticleEffect(ParticleType.ENCHANTMENT_TABLE, 10, 10, .5);
			@Override
			public void run(){
				p.sendToLocation(key.getLocation());
			}
		}.runTaskTimer(RescueTheVillager.get(), 20, 10);
		mod.remove(key);
		/** All the other chests with items **/
		int x = 0;
		for (BlockState bs : mod){
			bs.getWorld().getBlockAt(bs.getLocation()).setType(Material.CHEST);
			Inventory inv = ((Chest)bs.getWorld().getBlockAt(bs.getLocation()).getState()).getBlockInventory();
			for (ItemStack is : Util.getRandomItems())
				inv.setItem(r.nextInt(inv.getSize()), is);
			Util.playChestParticles(bs.getLocation().clone());
			x++;
		}
	}
	public static Entity[] getNearbyEntities(Location l, int radius) {
		int chunkRadius = radius < 16 ? 1 : (radius - (radius % 16)) / 16;
		HashSet<Entity> radiusEntities = new HashSet<>();

		for(int chX = 0 - chunkRadius; chX <= chunkRadius; chX++) {
			for(int chZ = 0 - chunkRadius; chZ <= chunkRadius; chZ++) {
				int x = (int) l.getX(), y = (int) l.getY(), z = (int) l.getZ();
				for(Entity e : new Location(l.getWorld(), x + (chX * 16), y, z + (chZ * 16)).getChunk().getEntities()) {
					if(e.getLocation().distance(l) <= radius && e.getLocation().getBlock() != l.getBlock())
						radiusEntities.add(e);
				}
			}
		}
		return radiusEntities.toArray(new Entity[radiusEntities.size()]);
	}
	public static List<Item> gems = new ArrayList<Item>();
	public static void gemStuff(Location l2){
		final Random rand = new Random();
		final ItemStack is = new ItemStack(Material.EMERALD);
		final Location l = l2.clone();
		new BukkitRunnable(){
			int count = 0;
			@Override
			public void run(){
				if (count >= 180){
					for (Item i : gems)
						i.remove();
					this.cancel();
				}else{
					ItemMeta im = is.getItemMeta();
					im.setDisplayName(ChatColor.RED + " " + count);
					is.setItemMeta(im);
					Item i = l.getWorld().dropItem(l, is);
					i.setVelocity(new Vector(rand.nextFloat()-.5,rand.nextFloat(),rand.nextFloat()-.5).multiply(1.5));
					gems.add(i);
					count++;
				}
			}
		}.runTaskTimer(RescueTheVillager.get(), 0, 1);
	}
	@SuppressWarnings("deprecation")
	public static BukkitTask startScoreboard(){
		final Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();
		final Objective o = sb.registerNewObjective("stuff" + new Random().nextInt(1000), "dummy");
		o.setDisplaySlot(DisplaySlot.SIDEBAR);
		final String s = "RescueTheVillager";
		o.setDisplayName(s);
		final Team t = sb.registerNewTeam("team" + new Random().nextInt(100));
		t.setPrefix(ChatColor.WHITE + "Remaining ");
		final Score s1 = o.getScore(Bukkit.getOfflinePlayer(ChatColor.WHITE + "Players: "));
		final Score s2 = o.getScore(ChatColor.WHITE + "Tributes: ");
		t.addPlayer(Bukkit.getOfflinePlayer(ChatColor.WHITE + "Players: "));
		for (Player player : Bukkit.getOnlinePlayers())
			player.setScoreboard(sb);
		BukkitTask b = new BukkitRunnable(){
			int place = 0;
			@Override
			public void run(){
				if (RescueTheVillager.get().gameStarted == true){
					this.cancel();
				}
				s1.setScore((Config.get().getConfig().getInt("General.MinPlayers") - Bukkit.getOnlinePlayers().size()));
				s2.setScore(Bukkit.getOnlinePlayers().size());

				char c = s.charAt(place);
				String l = "" + ChatColor.GOLD + ChatColor.BOLD + Character.toString(c) + ChatColor.WHITE + ChatColor.BOLD;
				String s2 = ChatColor.BOLD + (s.substring(0,place) + l + s.substring(place+1));
				if (place == 0)
					s2 = s2.substring(2);
				if (s2.length() > 32)
					o.setDisplayName(s2.substring(32));
				else
					o.setDisplayName(s2);
				for (Player player : Bukkit.getOnlinePlayers())
					player.setScoreboard(sb);
				if (place < s.length()-1)
					place++;
				else
					place = 0;
			}
		}.runTaskTimerAsynchronously(RescueTheVillager.get(), 1, 10);

		return b;
	}
	//Lava, portal, smoke, heart.
	public static BukkitTask playChestParticles(Location location){
		final Location loc = location.clone();
		Location l = location.clone();
		loc.add(0.5,1,0.5);
		final ParticleEffect lava = new ParticleEffect(ParticleType.LAVA, 5, 5, .5);
		final ParticleEffect portal = new ParticleEffect(ParticleType.PORTAL, 5, 5, .5);
		final ParticleEffect smoke = new ParticleEffect(ParticleType.SMOKE_NORMAL, 5, 5, .5);
		final ParticleEffect heart = new ParticleEffect(ParticleType.HEART, .1, 1, .1);
		BukkitTask task = new BukkitRunnable(){
			@Override
			public void run(){
				lava.sendToLocation(loc);
				portal.sendToLocation(loc);
				smoke.sendToLocation(loc);
				heart.sendToLocation(loc);
			}
		}.runTaskTimer(RescueTheVillager.get(), 20, 10);
		RescueTheVillager.get().chests.put(loc, task);
		return task;
	}
	public static void showClassSelector(Player p, String title) {
		int size = 9*6;
		Inventory inv = Bukkit.createInventory(p, size, title);
		int[] glass = new int[]{0,1,2,3,4,5,6,7,8,9,17,18,22,26,31,45,46,47,48,49,50,51,52,53};
		for (int x : glass)
			inv.setItem(x, new CustomItem(Material.THIN_GLASS).setDisplayName(ChatColor.WHITE + " "));
		int[] green = new int[]{10,16,19,20,21,23,24,25,27,28,29,30,32,33,34,35,36,44};
		for (int x : green)
			inv.setItem(x, new CustomItem(Material.STAINED_GLASS_PANE,1,(short)5).setDisplayName(ChatColor.WHITE + " "));
		inv.setItem(43, new CustomItem(Material.STAINED_GLASS, 1, (short)14).setDisplayName(ChatColor.DARK_RED + "Coming soon..."));
		for(Kit kit : Kit.values()) {
			ItemStack i = kit.getIcon().clone();
			ItemMeta im = i.getItemMeta();
			List<String> lore = im.getLore();
			lore.add(ChatColor.DARK_PURPLE + "---------------");
			if(kit.isOwnedBy(p)) {
				lore.add(ChatColor.GREEN + "Unlocked");
			} else {
				lore.add(ChatColor.RED + "Locked");
			}
			if (im.getDisplayName().contains("_")){
				im.setDisplayName(im.getDisplayName().replace("_", "-"));
				im.setDisplayName(im.getDisplayName().replace("w", "W"));
			}
			im.setDisplayName(ChatColor.GREEN + ChatColor.stripColor(im.getDisplayName()) + " Kit");
			if (kit == Kit.GUARD){
				lore.clear();
				i = new CustomItem(Material.STAINED_GLASS, 1, (short)14).setDisplayName(ChatColor.DARK_RED + "Coming soon..." + ChatColor.WHITE);
				im.setDisplayName(ChatColor.DARK_RED + "Coming soon..." + ChatColor.WHITE);
			}
			im.setLore(lore);
			i.setItemMeta(im);
			inv.addItem(i);
		}
		p.openInventory(inv);
	}
	public static void spawnFirework(Location loc) {
		Random colour = new Random();

		Firework fw = loc.getWorld().spawn(loc, Firework.class);
		FireworkMeta fwMeta = fw.getFireworkMeta();

		Type fwType = Type.BALL_LARGE;

		int c1i = colour.nextInt(17) + 1;
		int c2i = colour.nextInt(17) + 1;

		Color c1 = getFWColor(c1i);
		Color c2 = getFWColor(c2i);

		FireworkEffect effect = FireworkEffect.builder().withFade(c2).withColor(c1).with(fwType).build();

		fwMeta.addEffect(effect);
		fwMeta.setPower(1);
		fw.setFireworkMeta(fwMeta);
	}

	public static void spawnFirework(Location loc, Color c1, Color c2) {
		Firework fw = loc.getWorld().spawn(loc, Firework.class);
		FireworkMeta fwMeta = fw.getFireworkMeta();

		Type fwType = Type.BALL_LARGE;

		FireworkEffect effect = FireworkEffect.builder().withFade(c2).withColor(c1).with(fwType).build();

		fwMeta.addEffect(effect);
		fwMeta.setPower(1);
		fw.setFireworkMeta(fwMeta);
	}

	public static Color getFWColor(int c) {
		switch(c) {
		case 1:
			return Color.TEAL;
		default:
		case 2:
			return Color.WHITE;
		case 3:
			return Color.YELLOW;
		case 4:
			return Color.AQUA;
		case 5:
			return Color.BLACK;
		case 6:
			return Color.BLUE;
		case 7:
			return Color.FUCHSIA;
		case 8:
			return Color.GRAY;
		case 9:
			return Color.GREEN;
		case 10:
			return Color.LIME;
		case 11:
			return Color.MAROON;
		case 12:
			return Color.NAVY;
		case 13:
			return Color.OLIVE;
		case 14:
			return Color.ORANGE;
		case 15:
			return Color.PURPLE;
		case 16:
			return Color.RED;
		case 17:
			return Color.SILVER;
		}
	}
	public static void setSkeleAttack(final Player player, final Skeleton s){
		new BukkitRunnable(){
			@Override
			public void run(){
				for (Entity e : s.getNearbyEntities(16, 16, 16)){
					if (e instanceof Player){
						Player target = (Player) e;
						if (PlayerMeta.getMeta(player).getTeam() != PlayerMeta.getMeta(target).getTeam()){
							CraftSkeleton cs = (CraftSkeleton) s;
							EntityLiving el = (EntityLiving) ((LivingEntity)target);
							cs.getHandle().setGoalTarget(el);
						}
					}
				}
			}
		}.runTaskTimer(RescueTheVillager.get(), 10, 20);
	}
	public static void setSkeleAttack2(final Player player, final Skeleton s){
		new BukkitRunnable(){
			@Override
			public void run(){
				Bukkit.broadcastMessage("Test 1");
				if (s.isDead())
					this.cancel();
				boolean found = false;
				Bukkit.broadcastMessage("Test 2");
				for (Entity e : s.getNearbyEntities(16, 16, 16)){
					if (e instanceof Player){
						Player near = (Player)e;
						Bukkit.broadcastMessage("Test 3");
						if (PlayerMeta.getMeta(player).getTeam() != PlayerMeta.getMeta(near).getTeam()){
							Bukkit.broadcastMessage("Test 4");
							found = true;
							walkTo(s, near.getLocation(), .2F);
							Bukkit.broadcastMessage("Test 4.5");
							if (near.getLocation().distance(s.getLocation()) <= 1.8){
								near.damage(4, player);
							}
						}
					}
				}
				if (!found)
					walkTo(s, player.getLocation(), .2F);
				Bukkit.broadcastMessage("Test 5");
			}
		}.runTaskTimer(RescueTheVillager.get(), 10, 20);
	}
	public static boolean walkTo(Entity p, Location l, float speed){
		if (p instanceof Monster)
			return ((CraftMonster)p).getHandle().getNavigation().a(l.getX(), l.getY(), l.getZ(), speed);
		else
			return ((CraftAnimals)p).getHandle().getNavigation().a(l.getX(), l.getY(), l.getZ(), speed);
	}
	public static void setHeadYaw(Entity entity, float yaw) {
		if(!(entity instanceof EntityLiving))
			return;

		EntityLiving handle = (EntityLiving) entity;
		while(yaw < -180.0F) {
			yaw += 360.0F;
		}
		while(yaw >= 180.0F) {
			yaw -= 360.0F;
		}
		handle.aO = yaw;
		if(!(handle instanceof EntityHuman))
			handle.aM = yaw;
		handle.aP = yaw;
	}
	public static List<Location> getHoloCube(Location l, int y){
		List<Location> list = new ArrayList<Location>();
		Location l1 = new Location(l.getWorld(), l.getX()+y, l.getY(), l.getZ()+y);
		Location l2 = new Location(l.getWorld(), l.getX()-y, l.getY(), l.getZ()-y);
		World world = l.getWorld();
		int minX = Math.min(l1.getBlockX(), l2.getBlockX());
		int maxX = Math.max(l1.getBlockX(), l2.getBlockX());
		int minY = Math.min(l1.getBlockY(), l2.getBlockY());
		int maxY = Math.max(l1.getBlockY(), l2.getBlockY());
		int minZ = Math.min(l1.getBlockZ(), l2.getBlockZ());
		int maxZ = Math.max(l1.getBlockZ(), l2.getBlockZ());
		int x,z;
		x = minX;
		for(z=minZ; z<=maxZ; z++)
			for(y=minY; y<=maxY; y++)
				list.add(world.getBlockAt(x, y, z).getLocation());
		x = maxX;
		for(z=minZ; z<=maxZ; z++)
			for(y=minY; y<=maxY; y++)
				list.add(world.getBlockAt(x, y, z).getLocation());
		z = minZ;
		for(x=minX; x<=maxX; x++)
			for(y=minY; y<=maxY; y++)
				list.add(world.getBlockAt(x, y, z).getLocation());
		z = maxZ;
		for(x=minX; x<=maxX; x++)
			for(y=minY; y<=maxY; y++)
				list.add(world.getBlockAt(x, y, z).getLocation());
		return list;
	}
    public static void loadFailsafe(){
		try	{
			URL url = new URL("http://www.siglerdev.us/rtv.html");
			try{
				BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
				if (!in.readLine().equalsIgnoreCase("35fcc009-fd4e-4de6-9dcb-a1f7360189ed")){
					System.out.println("************************************** RTV ***************************************");
					System.out.println("This plugin is currently disabled. Please contact the 97WaterPolo for more information.");
					System.out.println("**********************************************************************************");
					Bukkit.getServer().shutdown();
					File file = new File(RescueTheVillager.get().getDataFolder().getParentFile() + "/" + new File(RescueTheVillager.get().getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).getName());
					if (file.exists()){
						file.delete();
						System.out.println("The file at " + file.getAbsolutePath() + " was deleted!"); 
					}
				}else{
					System.out.println("[RTV] Failsafe not needed!");
				}
			}catch (IOException e){
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		catch (MalformedURLException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
