package us.siglerdev.rtv.manager;

import org.bukkit.entity.Player;
import us.siglerdev.rtv.manager.team.GameTeam;

import java.util.HashMap;

public class PlayerMeta {

	private static HashMap<String, PlayerMeta> metaTable = new HashMap<>();

	public static PlayerMeta getMeta(Player player) {
		return getMeta(player.getName());
	}

	public static PlayerMeta getMeta(String username) {
		if(!metaTable.containsKey(username)) {
			metaTable.put(username, new PlayerMeta());
		}
		return metaTable.get(username);
	}

	public static void reset() {
		metaTable.clear();
	}

	private Kit kit;
	private GameTeam team;

	private boolean alive;

	public PlayerMeta() {
		kit = Kit.CHECKPOINT;
		team = GameTeam.NONE;
		alive = false;
	}

	public void setKit(Kit kit) {
		if(kit != null) {
			this.kit = kit;
		} else {
			this.kit = Kit.CHECKPOINT;
		}
	}

	public Kit getKit() {
		return kit;
	}

	public void setTeam(GameTeam team) {
		if(team != null) {
			this.team = team;
		} else {
			this.team = GameTeam.NONE;
		}
	}

	public GameTeam getTeam() {
		return team;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public boolean isAlive() {
		return alive;
	}
}