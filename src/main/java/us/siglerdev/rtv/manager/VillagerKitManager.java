package us.siglerdev.rtv.manager;

import net.minecraft.server.v1_7_R4.*;

import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_7_R4.CraftWorld;
import org.bukkit.craftbukkit.v1_7_R4.util.UnsafeList;

import net.minecraft.server.v1_7_R4.EntityHuman;
import net.minecraft.server.v1_7_R4.EntityVillager;
import net.minecraft.server.v1_7_R4.GenericAttributes;
import net.minecraft.server.v1_7_R4.PathfinderGoalLookAtPlayer;
import net.minecraft.server.v1_7_R4.PathfinderGoalRandomLookaround;
import net.minecraft.server.v1_7_R4.PathfinderGoalSelector;
import net.minecraft.server.v1_7_R4.World;

import java.lang.reflect.Field;
import java.util.Map;

public class VillagerKitManager extends EntityVillager {

	public VillagerKitManager(World world) {
		super(world);
		this.getAttributeInstance(GenericAttributes.d).setValue(0);
		this.setCustomName(ChatColor.translateAlternateColorCodes('&', "&b&lKit Selector"));
		this.setCustomNameVisible(true);
		this.setHealth(Integer.MAX_VALUE);

		try {
			Field bField = PathfinderGoalSelector.class.getDeclaredField("b");
			bField.setAccessible(true);
			Field cField = PathfinderGoalSelector.class.getDeclaredField("c");
			cField.setAccessible(true);
			bField.set(goalSelector, new UnsafeList<PathfinderGoalSelector>());
			bField.set(targetSelector, new UnsafeList<PathfinderGoalSelector>());
			cField.set(goalSelector, new UnsafeList<PathfinderGoalSelector>());
			cField.set(targetSelector, new UnsafeList<PathfinderGoalSelector>());
		} catch(Exception exc) {
			exc.printStackTrace();
			// This means that the name of one of the fields changed names or declaration and will have to be re-examined.
		}
	}

	public static Field mapStringToClassField, mapClassToStringField, mapClassToIdField, mapStringToIdField;

	static {
		try {
			mapStringToClassField = net.minecraft.server.v1_7_R4.EntityTypes.class.getDeclaredField("c");
			mapClassToStringField = net.minecraft.server.v1_7_R4.EntityTypes.class.getDeclaredField("d");
			mapClassToIdField = net.minecraft.server.v1_7_R4.EntityTypes.class.getDeclaredField("f");
			mapStringToIdField = net.minecraft.server.v1_7_R4.EntityTypes.class.getDeclaredField("g");
			mapStringToClassField.setAccessible(true);
			mapClassToStringField.setAccessible(true);
			mapClassToIdField.setAccessible(true);
			mapStringToIdField.setAccessible(true);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static void addCustomEntity(Class entityClass, String name, int id) {
		if(mapStringToClassField == null || mapStringToIdField == null || mapClassToStringField == null || mapClassToIdField == null) {
			return;
		} else {
			try {
				Map mapStringToClass = (Map) mapStringToClassField.get(null);
				Map mapStringToId = (Map) mapStringToIdField.get(null);
				Map mapClasstoString = (Map) mapClassToStringField.get(null);
				Map mapClassToId = (Map) mapClassToIdField.get(null);

				mapStringToClass.put(name, entityClass);
				mapStringToId.put(name, Integer.valueOf(id));
				mapClasstoString.put(entityClass, name);
				mapClassToId.put(entityClass, Integer.valueOf(id));

				mapStringToClassField.set(null, mapStringToClass);
				mapStringToIdField.set(null, mapStringToId);
				mapClassToStringField.set(null, mapClasstoString);
				mapClassToIdField.set(null, mapClassToId);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	@Override
	public void g(double d0, double d1, double d2) {
		return;
	}
	@Override
	public void move(double d0, double d1, double d2){
		return;
	}
	@Override
	public EntityAgeable createChild(EntityAgeable entityAgeable) {
		return null;
	}
}
