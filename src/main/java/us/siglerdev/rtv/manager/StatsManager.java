package us.siglerdev.rtv.manager;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class StatsManager {

	HashMap<UUID, Integer> kills = new HashMap<>();
	HashMap<UUID, Integer> deaths = new HashMap<>();

	public void increaseKills(Player player) {
		if(kills.containsKey(player.getUniqueId())) {
			kills.put(player.getUniqueId(), kills.get(player.getUniqueId()) + 1);
		} else {
			kills.put(player.getUniqueId(), 1);
		}
	}

	public void increaseDeaths(Player player) {
		if(deaths.containsKey(player.getUniqueId())) {
			deaths.put(player.getUniqueId(), deaths.get(player.getUniqueId()) + 1);
		} else {
			deaths.put(player.getUniqueId(), 1);
		}
	}

	public int getKills(Player player) {
		if(kills.containsKey(player.getUniqueId())) {
			return kills.get(player.getUniqueId());
		} else {
			return 0;
		}
	}

	public int getDeaths(Player player) {
		if(deaths.containsKey(player.getUniqueId())) {
			return deaths.get(player.getUniqueId());
		} else {
			return 0;
		}
	}
}