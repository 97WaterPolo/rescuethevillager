package us.siglerdev.rtv.manager;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import us.siglerdev.rtv.resource.Config;
import us.siglerdev.rtv.util.Util;

public enum Locale {

	ERROR_GENERAL(Config.get().getLocale().getString("ERROR_GENERAL")),
	ERROR_PLAYER_COMMAND(Config.get().getLocale().getString("ERROR_PLAYER_COMMAND")),
	ERROR_CONSOLE_COMMAND(Config.get().getLocale().getString("ERROR_CONSOLE_COMMAND")),
	JOIN(Config.get().getLocale().getString("JOIN")),
	JOIN_MESSAGE(Config.get().getLocale().getString("JOIN_MESSAGE")),
	GAME_STARTED(Config.get().getLocale().getString("GAME_STARTED")),
	GAME_FULL(Config.get().getLocale().getString("GAME_FULL")),
	GAME_FULL_PREMIUM(Config.get().getLocale().getString("GAME_FULL_PREMIUM")),
	NO_PERMISSION(Config.get().getLocale().getString("NO_PERMISSION")),
	FULL_MATCH(Config.get().getLocale().getString("FULL_MATCH")),
	FULL_MATCH_PREM(Config.get().getLocale().getString("FULL_MATCH_PREM")),;

	private String msg;

	private Locale(String msg) {
		this.msg = msg;
	}

	public String msg() {
		return ChatColor.translateAlternateColorCodes('&', msg);
	}

	public String msg(Player player) {
		msg = msg.replaceAll("%player%", player.getName());
		if(msg != null) {
			if(msg.contains("\n")) {
				String[] newMsg = msg.split("\n");
				for(int x = 0; x < newMsg.length - 1; x++)
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', newMsg[x]));
				return ChatColor.translateAlternateColorCodes('&', newMsg[newMsg.length - 1]);
			}
		} else
			return "NPE ERROR";
		return ChatColor.translateAlternateColorCodes('&', msg);
	}
}