package us.siglerdev.rtv.manager;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import us.siglerdev.rtv.event.SoulboundListener;
import us.siglerdev.rtv.manager.team.GameTeam;
import us.siglerdev.rtv.resource.CustomItem;

import java.util.ArrayList;
import java.util.List;

public enum Kit {

	CHECKPOINT(new ItemStack(Material.BEACON)) {
		{
			spawnItems.add(new ItemStack(Material.WOOD_SWORD));
			spawnItems.add(new CustomItem(Material.BEACON).setDisplayName(ChatColor.AQUA + "Checkpoint"));
			boot = new CustomItem(Material.CHAINMAIL_BOOTS);
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Place your checkpoint and");
			lore.add(ChatColor.GRAY + "come back to that position");
			lore.add(ChatColor.GRAY + "whenever you want.");
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Usage:");
			lore.add(ChatColor.GRAY + "- Place Beacon");
			lore.add(ChatColor.GRAY + "- Right-Click star to teleport");
		}
	},
	JUMPER(new ItemStack(Material.FEATHER)) {
		{
			spawnItems.add(new ItemStack(Material.WOOD_AXE));
			boot = new CustomItem(Material.IRON_BOOTS);
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Use your ability to jump");
			lore.add(ChatColor.GRAY + "and exceed the limits.");
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Usage:");
			lore.add(ChatColor.GRAY + "- Every 20 seconds, do a");
			lore.add(ChatColor.GRAY + "  a double space to jump!");
		}
	},
	NINJA(new ItemStack(Material.SUGAR)) {
		{
			spawnItems.add(new ItemStack(Material.GOLD_SWORD));
			spawnItems.add(new CustomItem(Material.SUGAR).setDisplayName(ChatColor.GREEN + "Sugar Esssence"));
			chest = new CustomItem(Material.LEATHER_CHESTPLATE);
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Use your amazing speed and play");
			lore.add(ChatColor.GRAY + "in a stealthy way hiding from");
			lore.add(ChatColor.GRAY + " your enemies.");
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Usage:");
			lore.add(ChatColor.GRAY + "- Right-Click sugar every w40sec");
			lore.add(ChatColor.GRAY + "  for a 20sec speed bost!");
			lore.add(ChatColor.GRAY + "- Stay shifted for 5sec to become");
			lore.add(ChatColor.GRAY + "  invisible to your enemies!");
		}
	},
	TANK(new CustomItem(Material.DIAMOND_CHESTPLATE).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)) {
		{
			spawnItems.add(new CustomItem(Material.DIAMOND_SWORD).setDisplayName(ChatColor.AQUA + "Olimpo's Force"));
			chest = new CustomItem(Material.DIAMOND_CHESTPLATE);
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Protect your teammates from");
			lore.add(ChatColor.GRAY + "your enemies and try to defend");
			lore.add(ChatColor.GRAY + " your position when your team");
			lore.add(ChatColor.GRAY + " has the key.");
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Usage:");
			lore.add(ChatColor.ITALIC + "No special abilities");
		}
	},
	/**  This class is disabled **/
	GUARD(new ItemStack(Material.BONE)) {
		{
			spawnItems.add(new ItemStack(Material.STONE_SWORD));
			spawnItems.add(new CustomItem(Material.BONE).setDisplayName(ChatColor.GREEN + "Guard Protector"));
			helm = new CustomItem(Material.CHAINMAIL_HELMET);
			chest = new CustomItem(Material.CHAINMAIL_CHESTPLATE);
			leg = new CustomItem(Material.CHAINMAIL_LEGGINGS);
			boot = new CustomItem(Material.CHAINMAIL_BOOTS);
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Guard the castle and defend");
			lore.add(ChatColor.GRAY + "it from the enemies!");
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Usage:");
			lore.add(ChatColor.GRAY + "- Right-Click the bone to");
			lore.add(ChatColor.GRAY + "  spawn a wolf!");
		}
	},
	WARRIOR(new CustomItem(Material.IRON_SPADE).addEnchant(Enchantment.FIRE_ASPECT, 1)) {
		{
			spawnItems.add(new CustomItem(Material.IRON_SPADE).setDisplayName(ChatColor.YELLOW + "Zeus's Power").addEnchant(Enchantment.FIRE_ASPECT, 1));
			leg = new CustomItem(Material.LEATHER_LEGGINGS);
			boot = new CustomItem(Material.LEATHER_BOOTS);
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Deal huge amounts of damage to");
			lore.add(ChatColor.GRAY + "your enemies!");
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Usage:");
			lore.add(ChatColor.GRAY + "- Right-Click spade every 30sec");
			lore.add(ChatColor.GRAY + "  to use deal 5 hearts AOE damage!");
		}
	},
	HEALER(new Potion(PotionType.REGEN, 1).toItemStack(1)) {
		{
			spawnItems.add(new ItemStack(Material.WOOD_SWORD));
			spawnItems.add(new CustomItem(Material.IRON_HOE).setDisplayName(ChatColor.LIGHT_PURPLE + "Atenea's Sceptre"));
			boot = new CustomItem(Material.GOLD_BOOTS);
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Heal your teammates and help");
			lore.add(ChatColor.GRAY + "them during the game.");
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Usage:");
			lore.add(ChatColor.GRAY + "- Right-Click a teammate every");
			lore.add(ChatColor.GRAY + "  20sec to heal them 4 hearts!");
		}
	},
	KNIGHT(new ItemStack(Material.IRON_BARDING)) {
		{
			spawnItems.add(new CustomItem(Material.IRON_SWORD).setDisplayName(ChatColor.DARK_PURPLE + "Ares's Spear"));
			spawnItems.add(new CustomItem(Material.HAY_BLOCK).setDisplayName(ChatColor.YELLOW + "Spawn Horse"));
			boot = new CustomItem(Material.CHAINMAIL_BOOTS);
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "More forward in position and");
			lore.add(ChatColor.GRAY + "win territory for your team.");
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Usage:");
			lore.add(ChatColor.ITALIC + "No special abilities");
		}
	},
	WIZARD(new CustomItem(Material.STICK)) {
		{
			spawnItems.add(new CustomItem(Material.WOOD_AXE));
			spawnItems.add(new CustomItem(Material.STICK).setDisplayName(ChatColor.DARK_AQUA + "The Elder Wand"));
			boot = new CustomItem(Material.LEATHER_BOOTS).setLeatherArmor(Color.PURPLE);
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Use you amazing power and");
			lore.add(ChatColor.GRAY + "kill your enemies.");
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Usage:");
			lore.add(ChatColor.GRAY + "- Right-Click wand every 10sec");
			lore.add(ChatColor.GRAY + "  to teleport 10 blocks!");
			lore.add(ChatColor.GRAY + "- Left-Click wand every 20sec");
			lore.add(ChatColor.GRAY + "  to call in a meteor strike!");
		}
	},
	FIREMAN(new ItemStack(Material.BLAZE_ROD)) {
		{
			spawnItems.add(new CustomItem(Material.GOLD_AXE));
			spawnItems.add(new CustomItem(Material.BLAZE_ROD).setDisplayName(ChatColor.GOLD + "Apolo's Force").addEnchant(Enchantment.DAMAGE_ALL, 1));
			helm = new CustomItem(Material.LEATHER_HELMET).setLeatherArmor(Color.fromRGB(216, 127, 51));
			chest = new CustomItem(Material.LEATHER_CHESTPLATE).setLeatherArmor(Color.fromRGB(222, 178, 51));
			leg = new CustomItem(Material.LEATHER_LEGGINGS).setLeatherArmor(Color.fromRGB(216, 127, 51));;
			boot = new CustomItem(Material.LEATHER_BOOTS).setLeatherArmor(Color.fromRGB(222, 178, 51));
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "The fires is with you and");
			lore.add(ChatColor.GRAY + "it will never abandon you.");
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Usage:");
			lore.add(ChatColor.GRAY + "- Left-Click the rod every");
			lore.add(ChatColor.GRAY + "  20sec to shoot a fireball!");
			lore.add(ChatColor.GRAY + "- Right-Click the rod every");
			lore.add(ChatColor.GRAY + "  50sec to summon a flame aura!");
		}
	},
	DARK_WIZARD(new CustomItem(Material.STICK).addGlow()){
		{
			spawnItems.add(new CustomItem(Material.WOOD_SWORD));
			spawnItems.add(new CustomItem(Material.STICK).setDisplayName(ChatColor.BLACK + "Tom Riddle's Wand").addEnchant(Enchantment.KNOCKBACK, 2));
			helm = new CustomItem(Material.LEATHER_HELMET).setLeatherArmor(Color.BLACK);
			chest = new CustomItem(Material.LEATHER_CHESTPLATE).setLeatherArmor(Color.BLACK);
			leg = new CustomItem(Material.LEATHER_LEGGINGS).setLeatherArmor(Color.BLACK);
			boot = new CustomItem(Material.LEATHER_BOOTS).setLeatherArmor(Color.BLACK);
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "You are a more powerful");
			lore.add(ChatColor.GRAY + "wizard, with more effective");
			lore.add(ChatColor.GRAY + " and amazing abilities.");
			lore.add(ChatColor.GRAY + "");
			lore.add(ChatColor.GRAY + "Usage:");
			lore.add(ChatColor.GRAY + "- Right-Click the wand to");
			lore.add(ChatColor.GRAY + "  summon a wither skeleton!");
			lore.add(ChatColor.GRAY + "- Left-Click the wand to");
			lore.add(ChatColor.GRAY + "  use your plasma wand!");
		}
	};

	static {
		for(Kit kit : values()) {
			kit.init();
		}
	}

	private ItemStack icon;
	List<String> lore = new ArrayList<>();
	List<ItemStack> spawnItems = new ArrayList<>();
	List<ItemStack> spawnArmor = new ArrayList<>();
	ItemStack helm, chest, leg, boot;

	Kit(ItemStack is) {
		icon = is.clone();
		ItemMeta meta = icon.getItemMeta();
		meta.setDisplayName(getName());
		icon.setItemMeta(meta);
	}

	private void init() {
		for(int i = 0; i < lore.size(); i++) {
			String s = lore.get(i);
			s = ChatColor.AQUA + s;
			lore.set(i, s);
		}
		ItemMeta meta = icon.getItemMeta();
		meta.setLore(lore);
		icon.setItemMeta(meta);
	}

	public static Kit getKit(String name) {
		for(Kit type : values()) {
			if(type.name().equalsIgnoreCase(name)) {
				return type;
			}
		}
		return null;
	}

	public void give(Player recipient) {
		PlayerInventory inv = recipient.getInventory();
		inv.clear();

		for(ItemStack item : spawnItems) {
			ItemStack toGive = item.clone();
			if (toGive.getType() == Material.HAY_BLOCK)
				toGive = new CustomItem(Material.HAY_BLOCK).setDisplayName(PlayerMeta.getMeta(recipient).getTeam().color() + recipient.getName() + "'s Horse");
			SoulboundListener.makeSoulbound(toGive);
			inv.addItem(toGive);
		}
		inv.addItem(SoulboundListener.makeSoulbound2(new ItemStack(Material.COOKED_BEEF, 16)));
		if(helm != null)
			inv.setHelmet(new ItemStack(helm));
		if(chest != null)
			inv.setChestplate(new ItemStack(chest));
		if(leg != null)
			inv.setLeggings(new ItemStack(leg));
		if(boot != null)
			inv.setBoots(new ItemStack(boot));

		for(ItemStack armor : inv.getArmorContents())
			if (armor != null && armor.getType() != Material.AIR)
				SoulboundListener.makeSoulbound(armor);
	}

	public String getName() {
		return name().substring(0, 1) + name().substring(1).toLowerCase();
	}

	public void setItemName(ItemStack is, String name) {
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(name);
		is.setItemMeta(meta);
	}

	public boolean isOwnedBy(Player player) {
		return player.isOp() || this == CHECKPOINT || player.hasPermission("rtv.class." + getName().toLowerCase());
	}

	public ItemStack getIcon() {
		return icon;
	}
}