package us.siglerdev.rtv.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;




import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import us.siglerdev.rtv.manager.team.GameTeam;

public class SiglerBoard
{
	private Scoreboard sb;
	private Objective o;
	private List<OfflinePlayer> offlinePlayers;
	private Team blue,white;
	public SiglerBoard(){
		sb = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
		o = sb.registerNewObjective("scoreboard", "dummy");
		o.setDisplaySlot(DisplaySlot.SIDEBAR);
		o.setDisplayName("" + ChatColor.AQUA + ChatColor.BOLD + "Rescue The Villager");
		offlinePlayers = new ArrayList<OfflinePlayer>();
		blue = sb.registerNewTeam("blue");
		white = sb.registerNewTeam("white");
		blue.setAllowFriendlyFire(false);
		white.setAllowFriendlyFire(false);
		blue.setPrefix(ChatColor.AQUA.toString());
		white.setPrefix(ChatColor.WHITE.toString());
	}
	public void updateTitle(String s){
		o.setDisplayName(s);
	}
	public void updateTeams(Player p){
		if (blue.hasPlayer(p))
			blue.removePlayer(p);
		if (white.hasPlayer(p))
			white.removePlayer(p);
		if (PlayerMeta.getMeta(p).getTeam() == GameTeam.BLUE && !blue.hasPlayer(p))
			blue.addPlayer(p);
		if (PlayerMeta.getMeta(p).getTeam() == GameTeam.WHITE && !white.hasPlayer(p))
			white.addPlayer(p);
	}
	public void updateTeams(){
		for (Player p : Bukkit.getOnlinePlayers()){
			if (blue.hasPlayer(p))
				blue.removePlayer(p);
			if (white.hasPlayer(p))
				white.removePlayer(p);
			if (PlayerMeta.getMeta(p).getTeam() == GameTeam.BLUE && !blue.hasPlayer(p))
				blue.addPlayer(p);
			if (PlayerMeta.getMeta(p).getTeam() == GameTeam.WHITE && !white.hasPlayer(p))
				white.addPlayer(p);
		}
	}
	public void setScore(String stat, int score){
		OfflinePlayer op = Bukkit.getOfflinePlayer(stat);
		o.getScore(op).setScore(score);
		if (!offlinePlayers.contains(op))
			offlinePlayers.add(op);
	}
	public void resetScores(){
		for (OfflinePlayer op : offlinePlayers)
			sb.resetScores(op);
	}
	public void reset(){
		resetScores();
		for (Player p : Bukkit.getOnlinePlayers()){
			//p.setScoreboard();
		}
	}
	public void setScoreboard(Player player){
		player.setScoreboard(sb);
	}
}
