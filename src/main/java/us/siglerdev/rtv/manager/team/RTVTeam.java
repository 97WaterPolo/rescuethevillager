package us.siglerdev.rtv.manager.team;

import org.bukkit.entity.Player;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.manager.team.api.TeamHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RTVTeam {

	private List<UUID> players = new ArrayList<>();

	private GameTeam gameTeam;
	private TeamHandler teamHandler;

	public RTVTeam(GameTeam gameTeam) {
		this.gameTeam = gameTeam;
	}

	public GameTeam getGameTeam() {
		return gameTeam;
	}

	public void addPlayer(Player player) {
		players.add(player.getUniqueId());
		PlayerMeta.getMeta(player).setTeam(gameTeam);
	}

	public void removePlayer(Player player) {
		players.remove(player.getUniqueId());
		PlayerMeta.getMeta(player).setTeam(GameTeam.NONE);
	}

	public List<UUID> getPlayers() {
		return players;
	}
}