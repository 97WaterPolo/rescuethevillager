package us.siglerdev.rtv.manager.team;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class TeamManager {

	private static TeamManager instance;

	public static TeamManager get() {
		if(instance == null) {
			instance = new TeamManager();
		}
		return instance;
	}

	private List<RTVTeam> teams = new ArrayList<>();

	public RTVTeam teamNONE = new RTVTeam(GameTeam.NONE);
	public RTVTeam teamBLUE = new RTVTeam(GameTeam.BLUE);
	public RTVTeam teamWHITE = new RTVTeam((GameTeam.WHITE));

	public TeamManager() {
		teams.add(teamNONE);
		teams.add(teamBLUE);
		teams.add(teamWHITE);
	}

	public void reset() {
		teams.clear();
	}

	public List<RTVTeam> getTeams() {
		return teams;
	}

	public void addPlayer(Player player, GameTeam team) {
		switch(team) {
			case NONE:
				teamNONE.addPlayer(player);
				break;
			case BLUE:
				teamBLUE.addPlayer(player);
				break;
			case WHITE:
				teamWHITE.addPlayer(player);
				break;
		}
	}

	public void removePlayer(Player player, GameTeam team) {
		switch(team) {
			case NONE:
				teamNONE.removePlayer(player);
				break;
			case BLUE:
				teamBLUE.removePlayer(player);
				break;
			case WHITE:
				teamWHITE.removePlayer(player);
				break;
		}
	}
}