package us.siglerdev.rtv.manager.team.api;

import us.siglerdev.rtv.manager.team.api.event.TeamAddPlayerEvent;
import us.siglerdev.rtv.manager.team.api.event.TeamRemovePlayerEvent;

public interface ITeamHandler {

	public void onTeamAddPlayerEvent(TeamAddPlayerEvent event);
	public void onTeamRemovePlayerEvent(TeamRemovePlayerEvent event);
}