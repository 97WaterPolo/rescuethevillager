package us.siglerdev.rtv.manager.team.api.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import us.siglerdev.rtv.manager.team.GameTeam;

public class TeamRemovePlayerEvent extends Event {

	private Player player;
	private GameTeam team;

	public TeamRemovePlayerEvent(Player player, GameTeam team) {
		this.player = player;
		this.team = team;
	}

	public Player getPlayer() {
		return player;
	}

	public GameTeam getTeam() {
		return team;
	}

	private HandlerList handlerList = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlerList;
	}
}