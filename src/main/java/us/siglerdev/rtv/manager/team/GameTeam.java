package us.siglerdev.rtv.manager.team;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.manager.team.api.TeamHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public enum GameTeam {

	WHITE, BLUE, NONE;

	private final ChatColor color;
	private List<Location> spawns;

	private TeamHandler teamHandler;

	GameTeam() {
		if(name().equals("NONE")) {
			color = ChatColor.GRAY;
		} else if(name().equals("BLUE")) {
			color = ChatColor.AQUA;
		} else {
			color = ChatColor.valueOf(name());
		}
		spawns = new ArrayList<>();
	}

	@Override
	public String toString() {
		return name().substring(0, 1) + name().substring(1).toLowerCase();
	}

	public String coloredName() {
		return color().toString() + toString();
	}

	public ChatColor color() {
		return color;
	}

	public void addSpawn(Location loc) {
		if(this != NONE) {
			spawns.add(loc);
		}
	}

	public Location getRandomSpawn() {
		if(!spawns.isEmpty() && this != NONE) {
			return spawns.get(new Random().nextInt(spawns.size()));
		}
		return null;
	}

	public List<Location> getSpawns() {
		return spawns;
	}

	public List<Player> getPlayers() {
		List<Player> players = new ArrayList<>();
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(PlayerMeta.getMeta(p).getTeam() == this && this != NONE) {
				players.add(p);
			}
		}
		return players;
	}

	public static GameTeam[] teams() {
		return new GameTeam[] {WHITE, BLUE};
	}

	public Color getColor(GameTeam gameTeam) {
		if(gameTeam == GameTeam.WHITE) return Color.WHITE;
		if(gameTeam == GameTeam.BLUE) return Color.BLUE;
		return null;
	}
}