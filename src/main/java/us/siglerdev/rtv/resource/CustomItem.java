package us.siglerdev.rtv.resource;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class CustomItem extends ItemStack {

	public CustomItem(Material material) {
		this.setType(material);
		this.setAmount(1);
	}

	public CustomItem(Material material, int amount) {
		this.setType(material);
		this.setAmount(amount);
	}

	public CustomItem(Material material, int amount, short durability) {
		this.setType(material);
		this.setAmount(amount);
		this.setDurability(durability);
	}

	public CustomItem setDisplayName(String displayName) {
		ItemMeta itemMeta = this.getItemMeta();
		itemMeta.setDisplayName(displayName);
		this.setItemMeta(itemMeta);
		return this;
	}

	public CustomItem setLore(String... lore) {
		List<String> list = new ArrayList<>();
		Collections.addAll(list, lore);

		ItemMeta itemMeta = this.getItemMeta();
		itemMeta.setLore(list);
		this.setItemMeta(itemMeta);
		return this;
	}
	public CustomItem setLeatherArmor(Color c){
		LeatherArmorMeta lam = (LeatherArmorMeta) this.getItemMeta();
		lam.setColor(c);
		this.setItemMeta(lam);
		return this;
	}
	public CustomItem addGlow(){
		Glow.addGlow(this);
		return this;
	}
	public CustomItem addEnchant(Enchantment enchantment, int level) {
		this.addUnsafeEnchantment(enchantment, level);
		return this;
	}

	public CustomItem addEnchants(Map<Enchantment, Integer> enchantments) {
		this.addUnsafeEnchantments(enchantments);
		return this;
	}
}