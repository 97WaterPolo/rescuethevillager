package us.siglerdev.rtv.resource;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class GameItems {

	public static ItemStack TEAM_CHOOSER = new CustomItem(Material.NETHER_STAR).setDisplayName(c("&aTeam Chooser &7(Right-Click)"));
	public static ItemStack TEAM_CHOOSER_BLUE = new CustomItem(Material.INK_SACK, 1, (short) 6).setDisplayName(c("&bBlue Team"))
		.setLore(c("&7Click here to join the &bBlue Team"));
	public static ItemStack TEAM_CHOOSER_WHITE = new CustomItem(Material.INK_SACK, 1, (short) 7).setDisplayName(c("&fWhite Team"))
		.setLore(c("&7Click here to join the &fWhite Team"));
	public static ItemStack KIT_SELECTOR = new CustomItem(Material.FEATHER).setDisplayName(c("&bKit Selector &7(Right-Click)"));
	public static ItemStack MAGIC_KEY = new CustomItem(Material.TRIPWIRE_HOOK,1,(short)1).setDisplayName(c("&f&kfiafuaifu&6&lMagic    Key&f&kfiaufuaifu"));

	private static String c(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}
}
