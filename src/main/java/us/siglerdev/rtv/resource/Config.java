package us.siglerdev.rtv.resource;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import us.siglerdev.rtv.RescueTheVillager;

import java.io.File;

public class Config {

	private static Config instance;

	public static Config get() {
		if(instance == null) {
			instance = new Config();
		}
		return instance;
	}

	private FileConfiguration config = RescueTheVillager.get().getConfig();
	private FileConfiguration locale = RescueTheVillager.get().msgYML;
	private FileConfiguration map = RescueTheVillager.get().mapYML;

	public FileConfiguration getConfig() {
		return config;
	}
	public FileConfiguration getLocale() {
		return locale;
	}
	public FileConfiguration getMap(){
		return map;
	}
	public void reloadConfig() {
		RescueTheVillager.get().reloadConfig();
	}

	public void reloadLocale() {
		locale = YamlConfiguration.loadConfiguration(new File(RescueTheVillager.get().getDataFolder(), "messages.yml"));
	}
}