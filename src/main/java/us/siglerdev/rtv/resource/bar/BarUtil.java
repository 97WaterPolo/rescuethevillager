package us.siglerdev.rtv.resource.bar;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitRunnable;

import us.siglerdev.rtv.RescueTheVillager;

import java.util.HashMap;

public class BarUtil implements Listener {

	private static BarUtil instance;

	private HashMap<String, FakeDragon> players = new HashMap<>();

	public static void init(RescueTheVillager plugin) {
		instance = new BarUtil();
		plugin.getServer().getPluginManager().registerEvents(instance, plugin);
	}

	public static void setMessageAndPercent(Player player, String message, float percent) {
		FakeDragon dragon = instance.getDragon(player, message);
		if(message.length() > 64) {
			dragon.name = message.substring(0, 63);
		} else {
			dragon.name = message;
		}
		dragon.health = percent * FakeDragon.MAX_HEALTH;
		instance.sendDragon(dragon, player);
	}

	public static void setMessage(Player player, String message) {
		FakeDragon dragon = instance.getDragon(player, message);
		if(message.length() > 64) {
			dragon.name = message.substring(0, 63);
		} else {
			dragon.name = message;
		}
		dragon.health = FakeDragon.MAX_HEALTH;
		instance.sendDragon(dragon, player);
	}

	public static void setPercent(Player player, float percent) {
		if(!instance.hasBar(player)) {
			return;
		}
		FakeDragon dragon = instance.getDragon(player, "");
		dragon.health = percent * FakeDragon.MAX_HEALTH;
		instance.sendDragon(dragon, player);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void PlayerLogout(PlayerQuitEvent event) {
		removeBar(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerKick(PlayerKickEvent event) {
		removeBar(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerTeleport(PlayerTeleportEvent event) {
		if(event.getTo() != null) {
			handleTeleport(event.getPlayer(), event.getTo().clone());
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		handleTeleport(event.getPlayer(), event.getRespawnLocation().clone());
	}

	private void handleTeleport(final Player player, final Location loc) {
		if(!hasBar(player)) {
			return;
		}
		new BukkitRunnable() {
			@Override
			public void run() {
				FakeDragon oldDragon = getDragon(player, "");
				Object destroyPacket = getDragon(player, "").getDestroyPacket();
				Util.sendPacket(player, destroyPacket);
				players.remove(player.getName());
				FakeDragon dragon = addDragon(player, loc, oldDragon.name);
				dragon.health = oldDragon.health;
				sendDragon(dragon, player);
			}
		}.runTaskLater(RescueTheVillager.get(), 60L); //Was 2L
	}

	private boolean hasBar(Player player) {
		return players.get(player.getName()) != null;
	}

	private void removeBar(Player player) {
		if(instance.hasBar(player)) {
			Util.sendPacket(player, instance.getDragon(player, "").getDestroyPacket());
			instance.players.remove(player.getName());
		}
	}

	private void sendDragon(FakeDragon dragon, Player player) {
		Object metaPacket = dragon.getMetaPacket(dragon.getWatcher());
		Object teleportPacket;
		if(((CraftPlayer) player).getHandle().playerConnection.networkManager.getVersion() == 47) {
			teleportPacket = dragon.getTeleportPacket(player.getLocation().add(0, 100, 0));
		} else {
			teleportPacket = dragon.getTeleportPacket(player.getLocation().add(0, -300, 0));
		}
		Util.sendPacket(player, metaPacket);
		Util.sendPacket(player, teleportPacket);
	}

	private FakeDragon getDragon(Player player, String message) {
		if(hasBar(player)) {
			return players.get(player.getName());
		} else {
			return addDragon(player, message);
		}
	}

	private FakeDragon addDragon(Player player, String message) {
		FakeDragon dragon;
		if(((CraftPlayer) player).getHandle().playerConnection.networkManager.getVersion() == 47) {
			dragon = Util.newDragon(message, player.getLocation().add(0, 100, 0));
		} else {
			dragon = Util.newDragon(message, player.getLocation().add(0, -300, 0));
		}
		Util.sendPacket(player, dragon.getSpawnPacket());
		players.put(player.getName(), dragon);
		return dragon;
	}

	private FakeDragon addDragon(Player player, Location loc, String message) {
		FakeDragon dragon;
		if(((CraftPlayer) player).getHandle().playerConnection.networkManager.getVersion() == 47) {
			dragon = Util.newDragon(message, loc.add(0, 100, 0));
		} else {
			dragon = Util.newDragon(message, loc.add(0, -300, 0));
		}
		Util.sendPacket(player, dragon.getSpawnPacket());
		players.put(player.getName(), dragon);
		return dragon;
	}
}