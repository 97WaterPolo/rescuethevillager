package us.siglerdev.rtv;

import net.md_5.bungee.api.ChatColor;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_7_R4.CraftWorld;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import de.slikey.effectlib.EffectLib;
import de.slikey.effectlib.EffectManager;
import us.siglerdev.rtv.event.player.PlayerDamage;
import us.siglerdev.rtv.event.player.PlayerQuit;
import us.siglerdev.rtv.resource.bar.BarUtil;
import us.siglerdev.rtv.classes.CheckPoint;
import us.siglerdev.rtv.classes.DarkWizard;
import us.siglerdev.rtv.classes.Fireman;
import us.siglerdev.rtv.classes.Guard;
import us.siglerdev.rtv.classes.Healer;
import us.siglerdev.rtv.classes.Jumper;
import us.siglerdev.rtv.classes.Knight;
import us.siglerdev.rtv.classes.Ninja;
import us.siglerdev.rtv.classes.Tank;
import us.siglerdev.rtv.classes.Warrior;
import us.siglerdev.rtv.classes.Wizard;
import us.siglerdev.rtv.event.GameListener;
import us.siglerdev.rtv.event.InventoryListener;
import us.siglerdev.rtv.event.SoulboundListener;
import us.siglerdev.rtv.event.VillagerListener;
import us.siglerdev.rtv.manager.team.GameTeam;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.manager.SiglerBoard;
import us.siglerdev.rtv.manager.StatsManager;
import us.siglerdev.rtv.manager.VillagerKitManager;
import us.siglerdev.rtv.manager.VillagerManager;
import us.siglerdev.rtv.event.player.PlayerChat;
import us.siglerdev.rtv.event.player.PlayerDeath;
import us.siglerdev.rtv.event.player.PlayerInteract;
import us.siglerdev.rtv.event.player.PlayerJoin;
import us.siglerdev.rtv.event.player.PlayerRespawn;
import us.siglerdev.rtv.resource.Config;
import us.siglerdev.rtv.resource.VoidGenerator;
import us.siglerdev.rtv.util.Util;

import java.io.File;
import java.util.HashMap;

public class RescueTheVillager extends JavaPlugin {

	private static RescueTheVillager instance;

	private StatsManager stats;

	public EffectManager eM;
	public boolean gameStarted,villagerReleased,endGame;
	public String rescuer;
	public YamlConfiguration msgYML;
	public YamlConfiguration mapYML;
	public BukkitTask lobbyScoreboard;
	public Villager villager;
	public HashMap<Location, BukkitTask> chests;
	public SiglerBoard sb;

	@Override
	public void onEnable() {
		instance = this;
		/** FailSafe **/
		Util.loadFailsafe();

		eM = new EffectManager(EffectLib.instance());
		chests = new HashMap<Location,BukkitTask>();
		BarUtil.init(this);
		sb = new SiglerBoard();

		endGame = false;
		gameStarted = false;
		villagerReleased = false;
		rescuer ="";

		stats = new StatsManager();
		/** Save the default configuration **/
		saveDefaultConfig();
		getConfig().options().copyHeader(true);
		getConfig().options().copyDefaults(true);
		msgYML = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "messages.yml"));
		mapYML = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "map.yml"));

		VillagerManager.addCustomEntity(VillagerManager.class, "Villager", 120);
		VillagerManager.addCustomEntity(VillagerKitManager.class, "Villager", 120);

		if(Bukkit.getWorld("lobby") == null) {
			WorldCreator wc = new WorldCreator("lobby");
			wc.generator(new VoidGenerator());
			Bukkit.createWorld(wc);
		}
		lobbyScoreboard = Util.startScoreboard();
		getCommand("t").setExecutor(new TestCommand());

		PluginManager pM = getServer().getPluginManager();
		/** Player Listeners **/
		pM.registerEvents(new PlayerJoin(), this);
		pM.registerEvents(new PlayerQuit(), this);
		pM.registerEvents(new PlayerDeath(), this);
		pM.registerEvents(new PlayerRespawn(), this);
		pM.registerEvents(new PlayerInteract(), this);
		pM.registerEvents(new PlayerDamage(), this);
		pM.registerEvents(new PlayerChat(), this);
		/** Game Listeners **/
		pM.registerEvents(new SoulboundListener(), this);
		pM.registerEvents(new GameListener(), this);
		pM.registerEvents(new InventoryListener(), this);
		pM.registerEvents(new VillagerListener(), this);
		/** Game Listeners **/
		pM.registerEvents(new CheckPoint(), this);
		pM.registerEvents(new Guard(), this);
		pM.registerEvents(new Healer(), this);
		pM.registerEvents(new Jumper(), this);
		pM.registerEvents(new Ninja(), this);
		pM.registerEvents(new Tank(), this);
		pM.registerEvents(new Warrior(), this);
		pM.registerEvents(new Knight(), this);
		pM.registerEvents(new Wizard(), this);
		pM.registerEvents(new Fireman(), this);
		pM.registerEvents(new DarkWizard(), this);
	}

	@Override
	public void onDisable() {
		for (BukkitTask b : chests.values())
			b.cancel();
		for (BlockState bs : PlayerInteract.ironBS)
			bs.update(true);
		for (BlockState bs : Util.bsO)
			bs.update(true);
		instance = null;
		eM.dispose();
	}
	@SuppressWarnings("deprecation")
	public void start(){
		gameStarted = true;
		Util.generateTeams();
		lobbyScoreboard.cancel();
		String blue = null,white = null;
		boolean w = false;
		boolean b = false;
		while (!b){
			int count = 0;
			for (Player p : Bukkit.getOnlinePlayers()){
				if (PlayerMeta.getMeta(p).getTeam() == GameTeam.BLUE){
					p.teleport(Util.parseLocation(Config.get().getMap().getString("World"),Config.get().getMap().getString("BlueSpawn")));
					blue = p.getName();
					b = true;
					break;
				}
				count++;
			}
			if (count >= Bukkit.getOnlinePlayers().size())
				break;
		}
		while (!w){
			int count = 0;
			for (Player p : Bukkit.getOnlinePlayers()){
				if (PlayerMeta.getMeta(p).getTeam() == GameTeam.WHITE){
					p.teleport(Util.parseLocation(Config.get().getMap().getString("World"), Config.get().getMap().getString("WhiteSpawn")));
					white = p.getName();
					w = true;
					break;
				}
				count++;
			}
			if (count >= Bukkit.getOnlinePlayers().size())
				break;
		}
		for (Player p : Bukkit.getOnlinePlayers()){
			if (PlayerMeta.getMeta(p).getTeam() == GameTeam.BLUE && !p.getName().equalsIgnoreCase(blue))
				p.teleport(Bukkit.getPlayer(blue).getLocation().clone().add(0,2,0));
			if (PlayerMeta.getMeta(p).getTeam() == GameTeam.WHITE && !p.getName().equalsIgnoreCase(white))
				p.teleport(Bukkit.getPlayer(white).getLocation().clone().add(0,2,0));
		}
		for (Player p : Bukkit.getOnlinePlayers()){
			p.setHealth(p.getMaxHealth());
			p.setFoodLevel(20);
			p.getInventory().clear();
			p.getInventory().setArmorContents(null);
			PlayerMeta.getMeta(p).getKit().give(p);
			sb.updateTeams(p);
			Util.updateGameScoreboard(p);
			Util.forceTabUpdate(p);
		}
		new BukkitRunnable(){@Override public void run(){World w = Bukkit.getWorld(Config.get().getMap().getString("World"));for (Entity e : w.getEntities())
			if (!(e instanceof Player) &&(e instanceof LivingEntity || e instanceof Item))e.remove();
		Util.spawnKitVillagers();Bukkit.getWorld(Config.get().getMap().getString("World")).setTime(0);}}.runTaskLater(this, 20);
		new BukkitRunnable(){
			@Override
			public void run(){
				World w = Bukkit.getWorld(Config.get().getMap().getString("World"));
				for (Entity e : w.getEntities())
					if (!(e instanceof Player || e instanceof Horse) && (e instanceof LivingEntity || e instanceof Item))
						e.remove();
				Util.spawnKitVillagers();
				Util.genChest();
				Location l = Util.parseLocation(w, Config.get().getMap().getString("VillagerSpawn"));
				VillagerManager v = new VillagerManager(((CraftWorld) w).getHandle());
				v.setPosition(l.getX(),l.clone().add(0,1,0).getY(),l.getZ());
				((CraftWorld)w).getHandle().addEntity(v);
				villager = (Villager) v.getBukkitEntity();
				System.out.println("LOC: " + v.getBukkitEntity().getLocation());
				Bukkit.getWorld(Config.get().getMap().getString("World")).setTime(0);
			}
		}.runTaskLater(instance, 260);
	}
	public void endGame(){
		for (BukkitTask b : chests.values())
			b.cancel();
		for (BlockState bs : PlayerInteract.ironBS)
			bs.update(true);
		for (BlockState bs : Util.bsO)
			bs.update(true);
		for (Item i : Util.gems)
			i.remove();
		chests.clear();
		PlayerInteract.ironBS.clear();
		Util.bsO.clear();
		Util.gems.clear();

		lobbyScoreboard = Util.startScoreboard();
		chests = new HashMap<Location,BukkitTask>();
		endGame = false;
		gameStarted = false;
		villagerReleased = false;
		InventoryListener.found = false;
		Util.cDown = false;
		rescuer ="";
		stats = new StatsManager();
		sb.reset();
		sb = new SiglerBoard();
		PlayerMeta.reset();

		for (Player player : Bukkit.getOnlinePlayers()){
			Util.resetPlayer(player);
			player.teleport(Util.parseLocation(Bukkit.getWorld("lobby"), Config.get().getMap().getString("LobbySpawn")));
			//TagAPI.refreshPlayer(player);
		}
	}
	public void triggerWin(final GameTeam g, final Player player){
		Bukkit.broadcastMessage(g.color() + StringUtils.capitalize(g.toString().toLowerCase()) + " team" + ChatColor.GOLD + " has won the game!");
		endGame = true;
		Util.gemStuff(player.getLocation());
		for (Player p : g.getPlayers())
			Util.spawnFirework(p.getLocation(), g.getColor(g), g.getColor(g));
		new BukkitRunnable(){
			int c = 3;
			@Override
			public void run(){
				if (c < 16){
					for (Location l : Util.getHoloCube(player.getLocation(), c))
						Util.spawnFirework(l);
					c+=3;
				}else
					this.cancel();
			}
		}.runTaskTimer(this, 10, 15);
		new BukkitRunnable(){
			@Override
			public void run(){
				World w = Bukkit.getWorld(Config.get().getMap().getString("World"));
				for (Entity e : w.getEntities())
					if (!(e instanceof Player) && e instanceof LivingEntity)
						e.remove();
				Util.gameRewards(g);
				endGame();
			}
		}.runTaskLater(this, 20*10);
	}
	public static RescueTheVillager get() {
		return instance;
	}

	public StatsManager getStats() {
		return stats;
	}
	public SiglerBoard getSiglerBoard(){
		return sb;
	}
}