package us.siglerdev.rtv.classes;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleFlightEvent;

import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.manager.Kit;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.util.Util;

import java.util.ArrayList;

public class Jumper implements Listener {

	public ArrayList<String> cooldown = new ArrayList<>();

	@EventHandler
	public void onFly(PlayerToggleFlightEvent event) {
		if(PlayerMeta.getMeta(event.getPlayer()).getKit() == Kit.JUMPER /*&& RescueTheVillager.get().gameStarted*/) {
			final Player player = event.getPlayer();
			if(player.getGameMode() != GameMode.CREATIVE) {
				if(!cooldown.contains(player.getName())) {
					if (Util.carryingVillager(player))
						return;
					event.setCancelled(true);
					player.setFallDistance(-8);
					player.setAllowFlight(false);
					player.setFlying(false);
					player.setVelocity(player.getLocation().getDirection().multiply(1.3).setY(.8));
					player.playSound(player.getLocation(), Sound.BAT_TAKEOFF, 1, 1);
					cooldown.add(player.getName());
					Bukkit.getScheduler().scheduleSyncDelayedTask(RescueTheVillager.get(), new Runnable() {
						@Override
						public void run() {
							cooldown.remove(player.getName());
							player.playSound(player.getLocation(), Sound.BAT_LOOP, 1.0F, 1.0F);
							player.setAllowFlight(true);
						}
					}, 20 * 20);
				}
				event.setCancelled(true);
				player.setAllowFlight(false);
				player.setFlying(false);
			}
		}
	}
}