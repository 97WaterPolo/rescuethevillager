package us.siglerdev.rtv.classes;

import java.util.HashMap;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.event.Listener;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import us.siglerdev.rtv.manager.Kit;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.manager.team.GameTeam;
import us.siglerdev.rtv.resource.ParticleEffect;
import us.siglerdev.rtv.resource.ParticleEffect.ParticleType;
import us.siglerdev.rtv.util.Util;

public class Healer implements Listener
{
	public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
	@EventHandler
	public void onInteractEntity(PlayerInteractEntityEvent event){
		Player player = event.getPlayer();
		if (!(event.getRightClicked() instanceof Player) || player.getItemInHand() == null || PlayerMeta.getMeta(player).getKit() != Kit.HEALER)
			return;
		Player target = (Player) event.getRightClicked();
		if (player.getItemInHand().getType() == Material.IRON_HOE){
			int cooldownTime = 20;
			if(cooldowns.containsKey(player.getName())) {
				long secondsLeft = ((cooldowns.get(player.getName())/1000)+cooldownTime) - (System.currentTimeMillis()/1000);
				if(secondsLeft>0) {
					player.sendMessage(ChatColor.GOLD + "Recharge>" + ChatColor.GRAY + " You can't do that for another " + ChatColor.RED + secondsLeft + ChatColor.GRAY + " seconds!");
					return;
				}
			}
			if (PlayerMeta.getMeta(player).getTeam() == PlayerMeta.getMeta(target).getTeam()){
				if (Util.carryingVillager(player))
					return;
				ParticleEffect p = new ParticleEffect(ParticleType.HEART, 10, 10, 1);
				p.sendToLocation(target.getEyeLocation().add(0,.5,0));
				if (target.getHealth() + 8 >= target.getMaxHealth())
					target.setHealth( + target.getMaxHealth());
				else
					target.setHealth(target.getHealth() + 8);
				player.sendMessage(ChatColor.GOLD + "Ability>" + ChatColor.GREEN + " Healing Activated!");
				cooldowns.put(player.getName(), System.currentTimeMillis());
			}
		}
	}
}
