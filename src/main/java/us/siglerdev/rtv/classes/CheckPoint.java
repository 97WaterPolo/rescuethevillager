package us.siglerdev.rtv.classes;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import us.siglerdev.rtv.event.SoulboundListener;
import us.siglerdev.rtv.manager.Kit;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.resource.CustomItem;
import us.siglerdev.rtv.resource.ParticleEffect;
import us.siglerdev.rtv.resource.ParticleEffect.ParticleType;
import us.siglerdev.rtv.util.Util;

import java.util.HashMap;
import java.util.UUID;

public class CheckPoint implements Listener {

	HashMap<UUID, Location> recall = new HashMap<>();

	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if(PlayerMeta.getMeta(player).getKit() == Kit.CHECKPOINT) {
			if(player.getItemInHand() != null) {
				if(player.getItemInHand().getType() == Material.NETHER_STAR) {
					if(recall.containsKey(player.getUniqueId())) {
						if (Util.carryingVillager(player))
							return;
						ParticleEffect p = new ParticleEffect(ParticleType.CLOUD, 10, 10, .2);
						p.sendToLocation(player.getLocation());
						player.teleport(recall.get(player.getUniqueId()));
						player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 20 * 2, 1));
						ParticleEffect p2 = new ParticleEffect(ParticleType.VILLAGER_HAPPY, 10, 10, 2);
						p2.sendToLocation(player.getLocation());
						player.playSound(player.getLocation(), Sound.PORTAL_TRAVEL, 1, 1);
						recall.remove(player.getUniqueId());
						player.setItemInHand(null);
					}
				}
			}
		}
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		if(PlayerMeta.getMeta(player).getKit() != Kit.CHECKPOINT || event.getBlock().getType() != Material.BEACON) {
			return;
		}
		if(!recall.containsKey(player.getUniqueId())) {
			event.getBlock().setType(Material.AIR);
			ParticleEffect p = new ParticleEffect(ParticleType.EXPLOSION_NORMAL, 10, 10, 2);
			p.sendToLocation(event.getBlock().getLocation());
			player.setItemInHand(SoulboundListener.makeSoulbound2(new CustomItem(Material.NETHER_STAR).setDisplayName(ChatColor.translateAlternateColorCodes('&', "Return to your checkpoint &7Right-Click"))));
			recall.put(player.getUniqueId(), event.getBlock().getLocation());
			player.playSound(player.getLocation(), Sound.BLAZE_BREATH, 1, 1);
		}
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		Player player = event.getEntity();
		if(recall.containsKey(player.getUniqueId())) {
			recall.remove(player.getUniqueId());
		}
	}

	@EventHandler
	public void onDeath(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		if(recall.containsKey(player.getUniqueId())) {
			recall.remove(player.getUniqueId());
		}
	}
}