package us.siglerdev.rtv.classes;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.manager.Kit;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.util.Util;

public class Knight implements Listener
{
	public static HashMap<String,Horse> horsey = new HashMap<String,Horse>();
	@EventHandler
	public void onRespawn(PlayerRespawnEvent event){
		final Player player = event.getPlayer();
		if (horsey.containsKey(player.getName())){
			horsey.get(player.getName()).remove();
			horsey.remove(player.getName());
		}
		if (PlayerMeta.getMeta(player).getKit() != Kit.KNIGHT)
			return;
	}
	@EventHandler
	public void onDeath(EntityDeathEvent event){
		if (event.getEntity().getType() == EntityType.HORSE){
			event.getDrops().clear();
			event.setDroppedExp(0);
			Horse h = (Horse) event.getEntity();
			h.getInventory().setArmor(null);
			h.getInventory().setSaddle(null);
			h.getInventory().clear();
			h.getInventory().setArmor(null);
			h.getInventory().setSaddle(null);
		}
	}
	@EventHandler
	public void onInteract(PlayerInteractEvent event){
		Player player = event.getPlayer();
		if (PlayerMeta.getMeta(player).getKit() != Kit.KNIGHT || player.getItemInHand() == null)
			return;
		if (player.getItemInHand().getType() == Material.HAY_BLOCK){
			player.setItemInHand(null);
			Horse h = (Horse) player.getWorld().spawnEntity(player.getLocation(), EntityType.HORSE);
			h.setAdult();h.setBreed(false);h.setCarryingChest(false);
			h.setCustomName("" + PlayerMeta.getMeta(player).getTeam().color() + ChatColor.BOLD + player.getName() + "'s Horse");
			h.setCustomNameVisible(true);
			h.getInventory().addItem(new ItemStack(Material.SADDLE));
			h.getInventory().addItem(new ItemStack(Material.IRON_BARDING));
			h.setTamed(true);
			h.setOwner(player);
			horsey.put(player.getName(), h);
		}
	}
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent event){
		if (event.getEntity() instanceof Horse && event.getDamager() instanceof Player){
			Player attacker = (Player)event.getDamager();
			Horse h = (Horse) event.getEntity();
			if (h.getOwner() instanceof Player){
				Player owner = (Player) h.getOwner();
				if (PlayerMeta.getMeta(attacker).getTeam() == PlayerMeta.getMeta(owner).getTeam())
					event.setCancelled(true);
			}
		}
	}
	@EventHandler
	public void onEnter(VehicleEnterEvent event){
		if (event.getVehicle() instanceof Horse && event.getEntered() instanceof Player){
			Player player = (Player) event.getEntered();
			Horse h = (Horse) event.getVehicle();
			if (player.getPassenger() != null){
				((LivingEntity) player.getPassenger()).setCustomNameVisible(true);
				Util.spawnFirework(player.getPassenger().getLocation(), Color.BLACK, Color.BLACK);
				player.getPassenger().getVehicle().eject();
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', PlayerMeta.getMeta(player).getTeam().color() + player.getName() + " &6has put down the &aVillager!"));
			}
			if (PlayerMeta.getMeta(player).getKit() != Kit.KNIGHT){
				event.setCancelled(true);
			}else{
				if (h.getOwner() instanceof Player){
					Player owner = (Player) h.getOwner();
					if (!owner.getName().equalsIgnoreCase(player.getName()))
						event.setCancelled(true);
				}
			}
		}

	}

}
