package us.siglerdev.rtv.classes;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import us.siglerdev.rtv.manager.Kit;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.manager.team.GameTeam;

public class Guard implements Listener
{
	/** Handles the spawning of the wolf **/
	@EventHandler
	public void onInteract(PlayerInteractEvent event){
		Player player = event.getPlayer();
		if (PlayerMeta.getMeta(player).getKit() != Kit.GUARD || player.getItemInHand() == null)
			return;
		if (player.getItemInHand().getType() == Material.BONE){
			player.setItemInHand(null);
			Wolf w = (Wolf) player.getWorld().spawnEntity(player.getLocation(), EntityType.WOLF);
			w.setCustomName(PlayerMeta.getMeta(player).getTeam().color() + player.getName() + "'s Wolf");
			w.setCustomNameVisible(true);
			w.setHealth(40);
			w.setOwner(player);
			w.setTamed(true);
			w.setRemoveWhenFarAway(false);
			w.setAdult();
			if (PlayerMeta.getMeta(player).getTeam() == GameTeam.BLUE)
				w.setCollarColor(DyeColor.BLUE);
			else
				w.setCollarColor(DyeColor.WHITE);
		}
	}

}