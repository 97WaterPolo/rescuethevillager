package us.siglerdev.rtv.classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Skeleton.SkeletonType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.manager.Kit;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.manager.team.GameTeam;
import us.siglerdev.rtv.resource.CustomItem;
import us.siglerdev.rtv.resource.ParticleEffect;
import us.siglerdev.rtv.resource.ParticleEffect.ParticleType;
import us.siglerdev.rtv.util.Util;

public class DarkWizard implements Listener
{
	public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
	@EventHandler
	public void onInteract(PlayerInteractEvent event){
		final Player player = event.getPlayer();
		if (PlayerMeta.getMeta(player).getKit() != Kit.DARK_WIZARD || player.getItemInHand() == null)
			return;
		if (player.getItemInHand().getType() == Material.STICK && (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK)){
			int cooldownTime = 2;
			if(cooldowns.containsKey(player.getName())) {
				long secondsLeft = ((cooldowns.get(player.getName())/1000)+cooldownTime) - (System.currentTimeMillis()/1000);
				if(secondsLeft>0) {
					player.sendMessage(ChatColor.GOLD + "Recharge>" + ChatColor.GRAY + " You can't do that for another " + ChatColor.RED + secondsLeft + ChatColor.GRAY + " seconds!");
					return;
				}
			}
			if (Util.carryingVillager(player))
				return;
			final Location start = player.getEyeLocation();
			final Location increase = start.getDirection().toLocation(start.getWorld());
			new BukkitRunnable(){
				int counter = 0;
				ParticleEffect pe = new ParticleEffect(ParticleType.SPELL_MOB, 5, 5, .2);
				@Override
				public void run() {
					if (counter >= 40) {
						cancel();
					} else {
						player.playSound(player.getLocation(), Sound.FUSE, 1, 1);
						for (int x = 0; x < 16; x++){
							Location point = start.add(increase);
							pe.sendToLocation(point);
							for (Entity e : Util.getNearbyEntities(point, 5))
								if (e instanceof Player)
									if (PlayerMeta.getMeta(((Player)e)).getTeam() != PlayerMeta.getMeta(player).getTeam())
										if (e.getLocation().distance(point) <= 2.5)
											((Player) e).damage(4, player);
						}
					}
					counter++;
				}
			}.runTaskTimer(RescueTheVillager.get(), 0, 1);
			cooldowns.put(player.getName(), System.currentTimeMillis());
			player.sendMessage(ChatColor.GOLD + "Ability>" + ChatColor.GREEN + " Plasma Wand Activated!");
		}
	}

	public HashMap<String, Long> cooldowns2 = new HashMap<String, Long>();
	public static HashMap<String,List<Skeleton>> skele = new HashMap<String,List<Skeleton>>();
	@EventHandler
	public void onInteract2(PlayerInteractEvent event){
		Player player = event.getPlayer();
		if (PlayerMeta.getMeta(player).getKit() != Kit.DARK_WIZARD || player.getItemInHand() == null)
			return;
		if (player.getItemInHand().getType() == Material.STICK && (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR)){
			int cooldownTime = 60;
			if(cooldowns2.containsKey(player.getName())) {
				long secondsLeft = ((cooldowns2.get(player.getName())/1000)+cooldownTime) - (System.currentTimeMillis()/1000);
				if(secondsLeft>0) {
					player.sendMessage(ChatColor.GOLD + "Recharge>" + ChatColor.GRAY + " You can't do that for another " + ChatColor.RED + secondsLeft + ChatColor.GRAY + " seconds!");
					return;
				}
			}
			if (Util.carryingVillager(player))
				return;
			if (skele.containsKey(player.getName())){
				for (Skeleton s : skele.get(player.getName()))
					s.remove();
				skele.remove(player.getName());
			}
			List<Skeleton> sList = new ArrayList<Skeleton>();
			for (int x = 0; x <2; x++){
				Skeleton s = (Skeleton) player.getWorld().spawnEntity(player.getLocation(), EntityType.SKELETON);
				s.setSkeletonType(SkeletonType.WITHER);
				s.setCustomName("" + ChatColor.BOLD + PlayerMeta.getMeta(player).getTeam().color() + player.getName() + "'s " + ChatColor.DARK_PURPLE + "Wither Skeleton");
				s.setCustomNameVisible(true);
				s.setMaxHealth(12);
				s.setHealth(s.getMaxHealth());
				s.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
				ItemStack is = new ItemStack(Material.SKULL_ITEM, 1, (short)SkullType.WITHER.ordinal());
				s.getEquipment().setHelmet(is);
				s.getEquipment().setHelmetDropChance(0);
				if (PlayerMeta.getMeta(player).getTeam() == GameTeam.BLUE)
					s.getEquipment().setItemInHand(new ItemStack(Material.DIAMOND_SWORD));
				else
					s.getEquipment().setItemInHand(new ItemStack(Material.IRON_SWORD));
				s.getEquipment().setItemInHandDropChance(0);
				if (PlayerMeta.getMeta(player).getTeam() == GameTeam.BLUE)
					s.getEquipment().setChestplate(new CustomItem(Material.LEATHER_CHESTPLATE).setLeatherArmor(Color.BLUE));
				else
					s.getEquipment().setChestplate(new CustomItem(Material.LEATHER_CHESTPLATE).setLeatherArmor(Color.WHITE));
				s.getEquipment().setChestplateDropChance(0);
				s.setSkeletonType(SkeletonType.WITHER);
				sList.add(s);
			}
			skele.put(player.getName(), sList);
			cooldowns2.put(player.getName(), System.currentTimeMillis());
			player.sendMessage(ChatColor.GOLD + "Ability>" + ChatColor.GREEN + " Wither Skeleton Activated!");
		}
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent event){
		if (event.getEntity() instanceof Skeleton && event.getDamager() instanceof Player){
			Player player = (Player) event.getDamager();
			Skeleton s = (Skeleton) event.getEntity();
			for (Map.Entry<String, List<Skeleton>> e : skele.entrySet()){
				for (Skeleton sK : e.getValue()){
					if (s.getUniqueId() == sK.getUniqueId()){
						if (PlayerMeta.getMeta(player).getTeam() == PlayerMeta.getMeta(Bukkit.getPlayer(e.getKey())).getTeam())
							event.setCancelled(true);
					}
				}
			}
		}
		if (event.getEntity() instanceof Player && event.getDamager() instanceof Skeleton){
			Player player = (Player) event.getEntity();
			Skeleton s = (Skeleton) event.getDamager();
			for (Map.Entry<String, List<Skeleton>> e : skele.entrySet()){
				for (Skeleton sK : e.getValue()){
					if (s.getUniqueId() == sK.getUniqueId()){
						player.damage(4.3, Bukkit.getPlayer(e.getKey()));
						event.setCancelled(true);
					}
				}
			}
		}
	}
	@EventHandler
	public void onDeath(EntityDeathEvent event){
		if (event.getEntity().getType() == EntityType.SKELETON){
			Skeleton s = (Skeleton) event.getEntity();
			s.getEquipment().clear();
			event.getDrops().clear();
		}
	}
	@EventHandler
	public void onTarget(EntityTargetEvent event){
		if (event.getEntity().getType() == EntityType.SKELETON){
			Skeleton s = (Skeleton) event.getEntity();
			if (s.getSkeletonType() == SkeletonType.WITHER){
				if (event.getTarget() instanceof Player){
					Player target = (Player) event.getTarget();
					for (Map.Entry<String, List<Skeleton>> e : skele.entrySet()){
						for (Skeleton sK : e.getValue()){
							if (s.getUniqueId() == sK.getUniqueId()){
								if (PlayerMeta.getMeta(target).getTeam() == PlayerMeta.getMeta(Bukkit.getPlayer(e.getKey())).getTeam())
									event.setCancelled(true);
							}
						}
					}
				}
			}
		}
	}
	@EventHandler
	public void onRespawn(PlayerRespawnEvent event){
		Player player = event.getPlayer();
		if (skele.containsKey(player.getName())){
			for (Skeleton s : skele.get(player.getName()))
				s.remove();
			skele.remove(player.getName());
		}
	}
}
