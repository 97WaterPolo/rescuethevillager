package us.siglerdev.rtv.classes;

import java.util.HashMap;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.EntityEffect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import de.slikey.effectlib.effect.ConeEffect;
import de.slikey.effectlib.effect.ShieldEffect;
import de.slikey.effectlib.util.ParticleEffect;
import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.manager.Kit;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.util.Util;

public class Fireman implements Listener
{
	public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
	@EventHandler
	public void onInteract(PlayerInteractEvent event){
		Player player = event.getPlayer();
		if (PlayerMeta.getMeta(player).getKit() != Kit.FIREMAN || player.getItemInHand() == null)
			return;
		if (player.getItemInHand().getType() == Material.BLAZE_ROD && (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK)){
			int cooldownTime = 40;
			if(cooldowns.containsKey(player.getName())) {
				long secondsLeft = ((cooldowns.get(player.getName())/1000)+cooldownTime) - (System.currentTimeMillis()/1000);
				if(secondsLeft>0) {
					player.sendMessage(ChatColor.GOLD + "Recharge>" + ChatColor.GRAY + " You can't do that for another " + ChatColor.RED + secondsLeft + ChatColor.GRAY + " seconds!");
					return;
				}
			}
			if (Util.carryingVillager(player))
				return;
			Fireball f = (Fireball) player.launchProjectile(Fireball.class);
			f.setFireTicks(0);
			f.setYield(0);
			f.setIsIncendiary(false);
			f.setTicksLived(6*20);
			f.setVelocity(f.getVelocity().multiply(1.45));
			player.sendMessage(ChatColor.GOLD + "Ability>" + ChatColor.GREEN + " Fireball Activated!");
			cooldowns.put(player.getName(), System.currentTimeMillis());
		}
	}
	public HashMap<String, Long> cooldowns2 = new HashMap<String, Long>();
	@EventHandler
	public void onInteract2(PlayerInteractEvent event){
		final Player player = event.getPlayer();
		if (PlayerMeta.getMeta(player).getKit() != Kit.FIREMAN || player.getItemInHand() == null)
			return;
		if (player.getItemInHand().getType() == Material.BLAZE_ROD && (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)){
			int cooldownTime = 50;
			if(cooldowns2.containsKey(player.getName())) {
				long secondsLeft = ((cooldowns2.get(player.getName())/1000)+cooldownTime) - (System.currentTimeMillis()/1000);
				if(secondsLeft>0) {
					player.sendMessage(ChatColor.GOLD + "Recharge>" + ChatColor.GRAY + " You can't do that for another " + ChatColor.RED + secondsLeft + ChatColor.GRAY + " seconds!");
					return;
				}
			}
			if (Util.carryingVillager(player))
				return;
			ShieldEffect c = new ShieldEffect(RescueTheVillager.get().eM);
			c.setEntity(player);
			c.offset = new Vector(0, -1, 0);
			c.iterations = 5*20;
			final int rad = c.radius;
			c.start();
			new BukkitRunnable(){
				int x = 0;
				@Override
				public void run(){
					if (x > 10)
						this.cancel();
					for (Entity e : player.getNearbyEntities(7, 7, 7)){
						if (e instanceof Player){
							Player target = (Player) e;
							if (player.getLocation().distance(target.getLocation()) <= rad){
								if (PlayerMeta.getMeta(player).getTeam() != PlayerMeta.getMeta(target).getTeam()){
									if (target.getHealth() -1 <= 0)
										target.damage(1000,player);
									else
										target.setHealth(target.getHealth()-1);
									target.playEffect(EntityEffect.HURT);
									for (Player p : Bukkit.getOnlinePlayers())
										p.playSound(target.getLocation(), Sound.HURT_FLESH, 1, 1);
								}
							}
						}
					}
					x++;
				}
			}.runTaskTimer(RescueTheVillager.get(), 0, 10);
			player.sendMessage(ChatColor.GOLD + "Ability>" + ChatColor.GREEN + " Fire Aura Activated!");
			cooldowns2.put(player.getName(), System.currentTimeMillis());
		}
	}
	@EventHandler
	public void damageHandler(EntityDamageByEntityEvent event){
		if (event.getEntity() instanceof Player){
			Player target = (Player) event.getEntity();
			if (event.getDamager() instanceof Fireball){
				Fireball fireball = (Fireball) event.getDamager();
				if (fireball.getShooter() instanceof Player){
					Player shooter = (Player) fireball.getShooter();
					if (PlayerMeta.getMeta(shooter).getTeam() != PlayerMeta.getMeta(target).getTeam()){
						event.setDamage(0);
						if (target.getHealth()-6 <= 0)
							target.damage(100, shooter);
						else
							target.setHealth(target.getHealth()-6);
						target.playEffect(EntityEffect.HURT);
						target.setFireTicks(40);
						target.playSound(target.getLocation(), Sound.FIRE, 1.0F, 1.0F);
					}
				}
			}
		}
	}
	@EventHandler
	public void onRespawn(PlayerRespawnEvent event){
		final Player player = event.getPlayer();
		new BukkitRunnable(){
			@SuppressWarnings("deprecation")
			@Override
			public void run(){
				if (PlayerMeta.getMeta(player).getKit() != Kit.FIREMAN){
					this.cancel();
				}
				player.playEffect(player.getLocation().add(0,.15,0), Effect.MOBSPAWNER_FLAMES, 0);
			}
		}.runTaskTimer(RescueTheVillager.get(), 20, 10);
	}
}