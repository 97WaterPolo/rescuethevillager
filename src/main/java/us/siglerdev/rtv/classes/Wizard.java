package us.siglerdev.rtv.classes;

import java.util.HashMap;

import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_7_R4.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_7_R4.PacketPlayOutWorldParticles;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.EntityEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftArrow;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.manager.Kit;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.resource.ParticleEffect;
import us.siglerdev.rtv.resource.ParticleEffect.ParticleType;
import us.siglerdev.rtv.util.Util;

public class Wizard implements Listener
{
	public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
	@EventHandler
	public void onInteract(PlayerInteractEvent event){
		Player player = event.getPlayer();
		if (PlayerMeta.getMeta(player).getKit() != Kit.WIZARD || player.getItemInHand() == null)
			return;
		if (player.getItemInHand().getType() == Material.STICK && (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR)){
			int cooldownTime = 10;
			if(cooldowns.containsKey(player.getName())) {
				long secondsLeft = ((cooldowns.get(player.getName())/1000)+cooldownTime) - (System.currentTimeMillis()/1000);
				if(secondsLeft>0) {
					player.sendMessage(ChatColor.GOLD + "Recharge>" + ChatColor.GRAY + " You can't do that for another " + ChatColor.RED + secondsLeft + ChatColor.GRAY + " seconds!");
					return;
				}
			}
			if (Util.carryingVillager(player))
				return;
			@SuppressWarnings("deprecation")
			Block b = player.getTargetBlock(null, 10);
			Location l = b.getLocation().clone();
			l.setYaw(player.getLocation().clone().getYaw());
			l.setPitch(player.getLocation().clone().getPitch());
			if (!b.getType().isSolid()){
				if (!b.getRelative(BlockFace.UP).getType().isSolid() || !b.getRelative(BlockFace.DOWN).getType().isSolid()){
					player.sendMessage(ChatColor.GOLD + "Ability>" + ChatColor.GREEN + " Teleport Activated!");
					player.playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);
					player.teleport(l);
					player.playEffect(player.getLocation(), Effect.ENDER_SIGNAL, 0);
					ParticleEffect p = new ParticleEffect(ParticleType.PORTAL, 10, 10, 2);
					p.sendToLocation(player.getLocation());
					cooldowns.put(player.getName(), System.currentTimeMillis());
				}else
					player.sendMessage(ChatColor.GRAY + "Danger: Unsafe teleportation area");
			}else
				player.sendMessage(ChatColor.GRAY + "Danger: Unsafe teleportation area");
		}
	}
	public HashMap<String, Long> cooldowns2 = new HashMap<String, Long>();
	@EventHandler
	public void onInteract2(PlayerInteractEvent event){
		final Player player = event.getPlayer();
		if (PlayerMeta.getMeta(player).getKit() != Kit.WIZARD || player.getItemInHand() == null)
			return;
		if (player.getItemInHand().getType() == Material.STICK && (event.getAction() == Action.LEFT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_AIR)){
			int cooldownTime = 20;
			if(cooldowns2.containsKey(player.getName())) {
				long secondsLeft = ((cooldowns2.get(player.getName())/1000)+cooldownTime) - (System.currentTimeMillis()/1000);
				if(secondsLeft>0) {
					player.sendMessage(ChatColor.GOLD + "Recharge>" + ChatColor.GRAY + " You can't do that for another " + ChatColor.RED + secondsLeft + ChatColor.GRAY + " seconds!");
					return;
				}
			}
			/**  Special Ability **/
			final Player target = getTargetPlayer(player);
			if (target == null){
				player.sendMessage(ChatColor.GOLD + "Ability>" + ChatColor.RED + " No nearby enemies!");
				return;
			}
			//final Location start = target.getLocation().clone();
			if (Util.carryingVillager(player))
				return;
			new BukkitRunnable(){
				ParticleEffect p = new ParticleEffect(ParticleType.SPELL_INSTANT, 5, 5, .1);
				int x1 = 0;
				@Override
				public void run(){
					if (x1 >= 10)
						this.cancel();
					Location start = target.getLocation().clone();
					p.sendToLocation(start.clone().add(0,x1,0));
					x1++;
				}
			}.runTaskTimer(RescueTheVillager.get(), 0, 1);
			new BukkitRunnable(){
				ParticleEffect p2 = new ParticleEffect(ParticleType.EXPLOSION_HUGE, 5, 5, .1);
				int x2= 10;
				@Override
				public void run(){
					Location start = target.getLocation().clone();
					if (x2 <= 0){
						p2.sendToLocation(start.clone());
						for (int g = 0; g <= 10; g++)
							start.getWorld().playEffect(start.clone().add(0,g,0), Effect.MOBSPAWNER_FLAMES, 0);
						if (target.getHealth() - 8 <= 0)
							target.damage(100, player);
						else
							target.setHealth(target.getHealth()-8);
						target.setVelocity(target.getVelocity().setY(.55).multiply(1.25));
						target.damage(0);
						for (Player p : Bukkit.getOnlinePlayers())
							p.playSound(target.getLocation(), Sound.EXPLODE, 1, 1);
						this.cancel();
					}
					Location loc = start.clone().add(0,x2,0);
					for (Player online : Bukkit.getOnlinePlayers()){
						online.playSound(target.getLocation(), Sound.NOTE_PLING, 1, 1);
						((CraftPlayer) online).getHandle().playerConnection
						.sendPacket(new PacketPlayOutWorldParticles("explode", (float) loc.getX(),(float) loc.getY(), (float) loc.getZ(), 1,1, 1,(float) 0, 6));
					}
					x2--;
				}
			}.runTaskTimer(RescueTheVillager.get(), 10, 1);
			player.sendMessage(ChatColor.GOLD + "Ability>" + ChatColor.GREEN + " Meteor Activated!");
			cooldowns2.put(player.getName(), System.currentTimeMillis());
		}
	}
	private Player getTargetPlayer(Player p){
		Player target = null;
		for (Entity e : p.getNearbyEntities(15, 15, 15)) {
			if (p.hasLineOfSight(e)){
				if (e instanceof Player){
					if (PlayerMeta.getMeta(p).getTeam() != PlayerMeta.getMeta((Player) e).getTeam())
						target = (Player) e;
				}
			}
			else {
				continue;
			}
		}
		return target;
	}
}
