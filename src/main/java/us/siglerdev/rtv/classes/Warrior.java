package us.siglerdev.rtv.classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.manager.Kit;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.manager.team.GameTeam;
import us.siglerdev.rtv.resource.ParticleEffect;
import us.siglerdev.rtv.resource.ParticleEffect.ParticleType;

public class Warrior implements Listener
{
	/** Handles the attack **/
	public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
	@EventHandler
	public void onInteract(PlayerInteractEvent event){
		final Player player = event.getPlayer();
		if (PlayerMeta.getMeta(player).getKit() != Kit.WARRIOR || player.getItemInHand() == null || event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK)
			return;
		if (player.getItemInHand().getType() == Material.IRON_SPADE){
			int cooldownTime = 30;
			if(cooldowns.containsKey(player.getName())) {
				long secondsLeft = ((cooldowns.get(player.getName())/1000)+cooldownTime) - (System.currentTimeMillis()/1000);
				if(secondsLeft>0) {
					player.sendMessage(ChatColor.GOLD + "Recharge>" + ChatColor.GRAY + " You can't do that for another " + ChatColor.RED + secondsLeft + ChatColor.GRAY + " seconds!");
					return;
				}
			}

			player.sendMessage(ChatColor.GOLD + "Ability>" + ChatColor.GREEN + " Zeus's Power Activated!");
			cooldowns.put(player.getName(), System.currentTimeMillis());
			new BukkitRunnable() {
				boolean done = false;
				@Override
				public void run() {
					for(final Location l : getCircle(player.getLocation().subtract(new Vector(0, 1, 0)), 2, 12)) {
						for (final Location l2 : getCircle(player.getLocation().subtract(new Vector(0, 1, 0)), 1, 12)){
							if (l2.getBlock().getType().isSolid()){
								final Material mat = l2.getBlock().getType();
								@SuppressWarnings("deprecation")
								final FallingBlock b = l2.getWorld().spawnFallingBlock(l2, l2.getBlock().getTypeId(), l2.getBlock().getData());
								b.setVelocity(new Vector(0.0D, 0.975D, 0.0D));
								b.setDropItem(false);
								new BukkitRunnable() {
									@Override
									public void run() {
										if(!b.isDead())
											b.remove();
										l2.getBlock().setType(mat);
									}
								}.runTaskLater(RescueTheVillager.get(), 33L);
							}
						}
						if (l.getBlock().getType().isSolid()){
							final Material mat = l.getBlock().getType();
							@SuppressWarnings("deprecation")
							final FallingBlock b = l.getWorld().spawnFallingBlock(l, l.getBlock().getTypeId(), l.getBlock().getData());
							b.setVelocity(new Vector(0.0D, 0.975D, 0.0D));
							b.setDropItem(false);
							new BukkitRunnable() {
								@Override
								public void run() {
									if(!b.isDead())
										b.remove();
									l.getBlock().setType(mat);
								}
							}.runTaskLater(RescueTheVillager.get(), 33L);
						}
						for(Entity nearby : getNearbyEntities(l, 5)) {
							if(nearby instanceof Player && !done) {
								Player allie = (Player) nearby;
								if (PlayerMeta.getMeta(player).getTeam() != PlayerMeta.getMeta(allie).getTeam()){
									allie.damage(0);
									allie.setVelocity(new Vector(0.0D, 0.75D, 0.0D));
									if (allie.getHealth() <= 10)
										allie.damage(100, player);
									else
										allie.damage(11, player);
									done = true;
									break;
								}
							}
						}
					}
				}
			}.runTaskLater(RescueTheVillager.get(), 2);
		}
	}
	/** Utils and Methods **/
	public ArrayList<Location> getCircle(Location center, double radius, int amount) {
		World world = center.getWorld();
		double increment = (2 * Math.PI) / amount;
		ArrayList<Location> locations = new ArrayList<Location>();
		for(int i = 0; i < amount; i++) {
			double angle = i * increment;
			double x = center.getX() + (radius * Math.cos(angle));
			double z = center.getZ() + (radius * Math.sin(angle));
			locations.add(new Location(world, x, center.getY(), z));
		}
		return locations;
	}
	public static Entity[] getNearbyEntities(Location l, int radius) {
		int chunkRadius = radius < 16 ? 1 : (radius - (radius % 16)) / 16;
		HashSet<Entity> radiusEntities = new HashSet<Entity>();

		for(int chX = 0 - chunkRadius; chX <= chunkRadius; chX++) {
			for(int chZ = 0 - chunkRadius; chZ <= chunkRadius; chZ++) {
				int x = (int) l.getX(), y = (int) l.getY(), z = (int) l.getZ();
				for(Entity e : new Location(l.getWorld(), x + (chX * 16), y, z + (chZ * 16)).getChunk().getEntities()) {
					if(e.getLocation().distance(l) <= radius && e.getLocation().getBlock() != l.getBlock())
						radiusEntities.add(e);
				}
			}
		}

		return radiusEntities.toArray(new Entity[radiusEntities.size()]);
	}
}
