package us.siglerdev.rtv.classes;

import java.util.ArrayList;
import java.util.HashMap;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.manager.Kit;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.util.Util;



public class Ninja implements Listener
{
	/** Speed Listener  **/
	public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
	@EventHandler
	public void onInteract(PlayerInteractEvent event){
		Player player = event.getPlayer();
		if (PlayerMeta.getMeta(player).getKit() != Kit.NINJA || player.getItemInHand() == null)
			return;
		if (player.getItemInHand().getType() == Material.SUGAR){
			int cooldownTime = 40;
			if(cooldowns.containsKey(player.getName())) {
				long secondsLeft = ((cooldowns.get(player.getName())/1000)+cooldownTime) - (System.currentTimeMillis()/1000);
				if(secondsLeft>0) {
					player.sendMessage(ChatColor.GOLD + "Recharge>" + ChatColor.GRAY + " You can't do that for another " + ChatColor.RED + secondsLeft + ChatColor.GRAY + " seconds!");
					return;
				}
			}
			if (Util.carryingVillager(player))
				return;
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20*20, 1));
			player.sendMessage(ChatColor.GOLD + "Ability>" + ChatColor.GREEN + " Speed Activated!");
			cooldowns.put(player.getName(), System.currentTimeMillis());
		}
	}
	/** Vanishing Listeners **/
	ArrayList<String> shift = new ArrayList<>();
	HashMap<String, Location> loc = new HashMap<>();

	@EventHandler
	public void onShift(PlayerToggleSneakEvent event) {
		final Player player = event.getPlayer();
		if(PlayerMeta.getMeta(player).getKit() == Kit.NINJA) {
			if(!shift.contains(player.getName())){
				new BukkitRunnable(){
					@Override
					public void run(){
						if(player.isSneaking()) {
							if(!shift.contains(player.getName())) {
								for(Player p : Bukkit.getOnlinePlayers()) {
									if (PlayerMeta.getMeta(player).getTeam() != PlayerMeta.getMeta(p).getTeam())
										p.hidePlayer(player);
								}
								player.playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);
								shift.add(player.getName());
								player.sendMessage(ChatColor.GREEN + "You have hidden from other players, don�t move and your enemies will not see you");
								loc.put(player.getName(), player.getLocation());
							}
						}
					}
				}.runTaskLater(RescueTheVillager.get(), 20*5);
			}else{
				shift.remove(player.getName());
				loc.remove(player.getName());
				player.sendMessage(ChatColor.RED + "You can now be seen by other players!");
				player.playSound(player.getLocation(), Sound.ENDERMAN_HIT, 1, 1);
				for(Player p : Bukkit.getOnlinePlayers()) {
					p.showPlayer(player);
				}
			}
		}
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if(PlayerMeta.getMeta(player).getKit() == Kit.NINJA) {
			if(shift.contains(event.getPlayer().getName())) {
				if(event.getTo().distance(loc.get(player.getName())) > 1) {
					shift.remove(player.getName());
					loc.remove(player.getName());
					player.sendMessage(ChatColor.RED + "You can now be seen by other players!");
					player.playSound(player.getLocation(), Sound.ENDERMAN_HIT, 1, 1);
					for(Player p : Bukkit.getOnlinePlayers()) {
						p.showPlayer(player);
					}
				}
			}
		}
	}

	@EventHandler
	public void onInteract(PlayerInteractEntityEvent event) {
		if(event.getRightClicked() instanceof Player) {
			Player player = (Player) event.getRightClicked();
			if(PlayerMeta.getMeta(player).getKit() == Kit.NINJA) {
				if(shift.contains(player.getName())) {
					shift.remove(player.getName());
					loc.remove(player.getName());
					player.sendMessage(ChatColor.RED + "You can now be seen by other players!");
					player.playSound(player.getLocation(), Sound.ENDERMAN_HIT, 1, 1);
					for(Player p : Bukkit.getOnlinePlayers()) {
						p.showPlayer(player);
					}
				}
			}
		}
		if(event.getPlayer() != null) {
			Player player = event.getPlayer();
			if(PlayerMeta.getMeta(player).getKit() == Kit.NINJA) {
				if(shift.contains(player.getName())) {
					shift.remove(player.getName());
					loc.remove(player.getName());
					player.sendMessage(ChatColor.RED + "You can now be seen by other players!");
					player.playSound(player.getLocation(), Sound.ENDERMAN_HIT, 1, 1);
					for(Player p : Bukkit.getOnlinePlayers()) {
						p.showPlayer(player);
					}
				}
			}
		}
	}

	@EventHandler
	public void onInteractEntity(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if(isAction(event.getAction())) {
			if(shift.contains(player.getName())) {
				shift.remove(player.getName());
				loc.remove(player.getName());
				player.sendMessage(ChatColor.RED + "You can now be seen by other players!");
				player.playSound(player.getLocation(), Sound.ENDERMAN_HIT, 1, 1);
				for(Player p : Bukkit.getOnlinePlayers()) {
					p.showPlayer(player);
				}
			}
		}
	}

	@EventHandler
	public void NINJADamage(EntityDamageByEntityEvent event) {
		if(event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
			Player damager = (Player) event.getDamager();
			Player target = (Player) event.getEntity();
			if(PlayerMeta.getMeta(damager).getKit() == Kit.NINJA || PlayerMeta.getMeta(target).getKit() == Kit.NINJA) {
				if(shift.contains(damager.getName())) {
					shift.remove(damager.getName());
					loc.remove(target.getName());
					damager.sendMessage(ChatColor.RED + "You can now be seen by other players!");
					damager.playSound(damager.getLocation(), Sound.ENDERMAN_HIT, 1, 1);
					for(Player p : Bukkit.getOnlinePlayers()) {
						p.showPlayer(damager);
					}
				}
				if(shift.contains(target.getName())) {
					shift.remove(target.getName());
					loc.remove(target.getName());
					target.sendMessage(ChatColor.RED + "You can now be seen by other players!");
					target.playSound(target.getLocation(), Sound.ENDERMAN_HIT, 1, 1);
					for(Player p : Bukkit.getOnlinePlayers()) {
						p.showPlayer(target);
					}
				}
			}
		}
	}
	public boolean isAction(Action a) {
		switch(a) {
		case LEFT_CLICK_AIR:
		case LEFT_CLICK_BLOCK:
		case RIGHT_CLICK_AIR:
		case RIGHT_CLICK_BLOCK:
			return true;
		default:
			return false;
		}
	}

}
