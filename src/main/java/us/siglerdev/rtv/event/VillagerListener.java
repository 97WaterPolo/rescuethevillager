package us.siglerdev.rtv.event;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.scheduler.BukkitRunnable;

import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.manager.team.GameTeam;
import us.siglerdev.rtv.resource.Config;
import us.siglerdev.rtv.resource.ParticleEffect;
import us.siglerdev.rtv.resource.ParticleEffect.ParticleType;
import us.siglerdev.rtv.util.Util;

public class VillagerListener implements Listener {

	@EventHandler
	public void onInteract(PlayerInteractEntityEvent event){
		if (event.getRightClicked().getType() == EntityType.VILLAGER){
			if (((LivingEntity)event.getRightClicked()).getCustomName().contains("Kit")){
				Util.showClassSelector(event.getPlayer(), "Select Kit");
				event.setCancelled(true);
				return;
			}
			if (event.getRightClicked().getVehicle() != null || RescueTheVillager.get().villagerReleased == false || event.getPlayer().getVehicle() != null){
				event.setCancelled(true);
				return;
			}
			if (RescueTheVillager.get().endGame){
				event.setCancelled(true);
				return;
			}
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', PlayerMeta.getMeta(event.getPlayer()).getTeam().color()+ event.getPlayer().getName() + " &6is carrying the &aVillager!"));
			event.getPlayer().setPassenger(event.getRightClicked());
			((LivingEntity)event.getRightClicked()).setCustomName("" + ChatColor.GREEN + ChatColor.BOLD+ "Villager");
			if (((LivingEntity)event.getRightClicked()).getCustomName() != null)
				((LivingEntity)event.getRightClicked()).setCustomNameVisible(false);

			final Player player = event.getPlayer();
			new BukkitRunnable() {
				@Override
				public void run() {
					if(player.getPassenger() != null) {
						if (player.getLocation().distance(Util.parseLocation(player.getWorld(), Config.get().getMap().getString("WhiteHome"))) <= 1.5 &&
								PlayerMeta.getMeta(player).getTeam() == GameTeam.WHITE){
							Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&f" + player.getName() + " &6has saved the &aVillager!"));
							RescueTheVillager.get().triggerWin(GameTeam.WHITE,player);
							((LivingEntity) player.getPassenger()).setCustomNameVisible(true);
							player.getPassenger().getVehicle().eject();
						}
						if (player.getLocation().distance(Util.parseLocation(player.getWorld(), Config.get().getMap().getString("BlueHome"))) <= 1.5 &&
								PlayerMeta.getMeta(player).getTeam() == GameTeam.BLUE){
							Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&b" + player.getName() + " &6has saved the &aVillager!"));
							RescueTheVillager.get().triggerWin(GameTeam.BLUE,player);
							((LivingEntity) player.getPassenger()).setCustomNameVisible(true);
							player.getPassenger().getVehicle().eject();
						}
						ParticleEffect p = new ParticleEffect(ParticleType.VILLAGER_HAPPY, 10, 10, 3);
						p.sendToLocation(player.getEyeLocation().subtract(0, .5, 0));
						if(player.isSneaking()) {
							((LivingEntity) player.getPassenger()).setCustomNameVisible(true);
							player.getPassenger().getVehicle().eject();
							Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', PlayerMeta.getMeta(player).getTeam().color() + player.getName() + " &6has put down the &aVillager!"));
							Util.spawnFirework(player.getLocation(), Color.RED, Color.BLACK);
							this.cancel();
						}
					} else {
						this.cancel();
					}
				}
			}.runTaskTimer(RescueTheVillager.get(), 2, 2);

			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onDamage(EntityDamageEvent event) {
		if(event.getEntity().getType() == EntityType.VILLAGER) {
			if (event.getCause() == DamageCause.VOID)
				event.getEntity().teleport(Util.parseLocation(event.getEntity().getWorld(), Config.get().getMap().getString("VillagerSpawn")));
			if (RescueTheVillager.get().villagerReleased)
				event.setDamage(0);
			else
				event.setCancelled(true);
		}
		if(event.getEntity() instanceof Player) {
			final Player player = (Player) event.getEntity();
			if(player.getPassenger() != null) {
				((LivingEntity) player.getPassenger()).setCustomNameVisible(true);
				Util.spawnFirework(player.getPassenger().getLocation(), Color.BLACK, Color.BLACK);
				player.getPassenger().getVehicle().eject();
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', PlayerMeta.getMeta(player).getTeam().color() + player.getName() + " &6has dropped down the &aVillager!"));
			}
		}
	}
}
