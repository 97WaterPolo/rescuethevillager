package us.siglerdev.rtv.event;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.kitteh.tag.TagAPI;

import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.event.player.PlayerRespawn;
import us.siglerdev.rtv.manager.team.GameTeam;
import us.siglerdev.rtv.manager.Kit;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.resource.GameItems;
import us.siglerdev.rtv.util.Util;

public class InventoryListener implements Listener{
	@EventHandler
	public void onOpen(InventoryOpenEvent event){
		InventoryHolder holder = event.getInventory().getHolder();
		if(holder != null && holder instanceof Chest) {
			Chest chest = (Chest) holder;
			for (Location l : RescueTheVillager.get().chests.keySet()){
				if (l.distance(chest.getBlock().getLocation()) <= 1.5){
					RescueTheVillager.get().chests.get(l).cancel();
				}
			}
		}
	}
	public static boolean found = false;
	@EventHandler
	public void onKeyTake(InventoryCloseEvent event){
		Player player = (Player) event.getPlayer();
		for (ItemStack is : player.getInventory().getContents()){
			if (is != null){
				if (is.getType() == GameItems.MAGIC_KEY.getType()){
					if (is.getItemMeta().getDisplayName().contains(GameItems.MAGIC_KEY.getItemMeta().getDisplayName())){
						if (!found){
							Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', PlayerMeta.getMeta(player).getTeam().color()+ player.getName() +" &6has found the &bKey!"));
							found = true;
						}
					}
				}
			}
		}
	}
	@EventHandler
	public void onKeyClick(InventoryClickEvent event){
		if (event.getSlotType() == SlotType.OUTSIDE || event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR)
			return;
		if(event.getRawSlot() < event.getInventory().getSize()) {
			if (event.getCurrentItem().getType() == Material.GOLDEN_APPLE)
				event.setCancelled(true);
		}
	}
	@EventHandler
	public void onTeamClick(InventoryClickEvent event){
		if (event.getSlotType() == SlotType.OUTSIDE || event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR)
			return;
		if (event.getInventory().getName().startsWith("Select Team")){
			Player player = (Player) event.getWhoClicked();
			if (event.getCurrentItem().getItemMeta().getDisplayName().contains("Blue")){
				if (GameTeam.BLUE.getPlayers().size() <= GameTeam.WHITE.getPlayers().size() + 1){
					PlayerMeta.getMeta(player).setTeam(GameTeam.BLUE);
					player.sendMessage(ChatColor.AQUA + "You have joined the BLUE team!");
					RescueTheVillager.get().getSiglerBoard().updateTeams(player);
					Util.forceTabUpdate(player);

				}else
					player.sendMessage(ChatColor.DARK_RED + "You can not join that team, it is currently full!");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().contains("White")){
				if (GameTeam.WHITE.getPlayers().size() <= GameTeam.BLUE.getPlayers().size() + 1){
					PlayerMeta.getMeta(player).setTeam(GameTeam.WHITE);
					player.sendMessage("You have joined the WHITE team!");
					RescueTheVillager.get().getSiglerBoard().updateTeams(player);
					Util.forceTabUpdate(player);
				}else
					player.sendMessage(ChatColor.DARK_RED + "You can not join that team, it is currently full!");
			}
			//TagAPI.refreshPlayer(player);
			event.setCancelled(true);
			player.closeInventory();
		}
	}
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		Inventory inv = e.getInventory();
		Player player = (Player) e.getWhoClicked();

		if(e.getSlotType() == SlotType.OUTSIDE) {
			return;
		}
		if(inv.getTitle().startsWith("Select Kit")) {
			if(e.getCurrentItem() != null) {
				if(e.getCurrentItem().getType() == Material.AIR) {
					return;
				}
				if (e.getCurrentItem().getType().toString().contains("GLASS")){
					e.setCancelled(true);
					return;
				}
				player.closeInventory();
				e.setCancelled(true);
				String name = e.getCurrentItem().getItemMeta().getDisplayName().split(" ")[0];
				if (name.toUpperCase().contains("DARK"))
					name = "Dark_Wizard";
				PlayerMeta meta = PlayerMeta.getMeta(player);
				if (name.toUpperCase().contains("GUARD")){
					player.sendMessage(ChatColor.DARK_RED + "This kit is disabled!");
					e.setCancelled(true);
					return;
				}
				if(!Kit.valueOf(ChatColor.stripColor(name).toUpperCase())
						.isOwnedBy(player)) {
					player.sendMessage(ChatColor.RED + "You do not own this kit.");
					return;
				}

				meta.setKit(Kit.getKit(ChatColor.stripColor(name)));
				player.sendMessage(ChatColor.AQUA + "You selected: " + ChatColor.stripColor(name.replace("_", "-")));
				player.setAllowFlight(false);
				player.setFlying(false);

				if(!player.getWorld().getName().equalsIgnoreCase("lobby")) {
					PlayerRespawn.bypass.add(player.getName());
					player.setHealth(0);
				}

				if(meta.getKit() == Kit.JUMPER) {
					if(PlayerMeta.getMeta(player).getKit() == Kit.JUMPER) {
						if((player.getGameMode() != GameMode.CREATIVE)/*&& RescueTheVillager.get().gameStarted*/) {
							player.setAllowFlight(true);
						}
					}
				}
			}
		}
	}
}
