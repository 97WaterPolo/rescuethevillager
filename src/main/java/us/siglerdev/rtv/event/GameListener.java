package us.siglerdev.rtv.event;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Skeleton.SkeletonType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.kitteh.tag.AsyncPlayerReceiveNameTagEvent;

import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.resource.Config;
import us.siglerdev.rtv.util.Util;

public class GameListener implements Listener
{
	@EventHandler
	public void onBreak(BlockBreakEvent event){
		if (RescueTheVillager.get().gameStarted == true)
			event.setCancelled(true);
	}
	@EventHandler
	public void onPlace(BlockPlaceEvent event){
		if (RescueTheVillager.get().gameStarted == true)
			event.setCancelled(true);
	}
	@EventHandler
	public void onSpawn(CreatureSpawnEvent event){
		if (event.getEntityType() == EntityType.ZOMBIE || 
				event.getEntityType() == EntityType.CREEPER || 
				event.getEntityType() == EntityType.ENDERMAN || 
				event.getEntityType() == EntityType.SPIDER || 
				event.getEntityType() == EntityType.WITCH || 
				event.getEntityType() == EntityType.SLIME || 
				event.getEntityType() == EntityType.CAVE_SPIDER)
			event.setCancelled(true);

		if (event.getEntityType() == EntityType.SKELETON){
			if (event.getSpawnReason() != SpawnReason.CUSTOM && event.getSpawnReason() != SpawnReason.DEFAULT)
				event.setCancelled(true); 
		}
	}
	@EventHandler
	public void onFire(EntityCombustEvent event){
		if (event.getEntityType() == EntityType.SKELETON){
			if (((Skeleton)event.getEntity()).getSkeletonType() == SkeletonType.WITHER)
				event.setCancelled(true);
		}
	}
	@EventHandler
	public void onPickup(PlayerPickupItemEvent event){
		if (RescueTheVillager.get().endGame)
			event.setCancelled(true);
	}
	@EventHandler
	public void onEntityChangeBlock(EntityChangeBlockEvent event) {
		if (event.getEntityType() == EntityType.FALLING_BLOCK) {
			event.setCancelled(true);
		}
	}
	@EventHandler
	public void onPing(ServerListPingEvent event){
		event.setMaxPlayers(Config.get().getConfig().getInt("General.MaxPlayers"));
		if (RescueTheVillager.get().gameStarted)
			event.setMotd("In-Game");
		else
			event.setMotd("Starting");
	}
	@EventHandler
	public void onUnload(ChunkUnloadEvent event){
		Location wSpawn = Util.parseLocation(Config.get().getMap().getString("World"), Config.get().getMap().getString("WhiteSpawn"));
		Location bSpawn = Util.parseLocation(Config.get().getMap().getString("World"), Config.get().getMap().getString("BlueSpawn"));
		if (event.getChunk() == wSpawn.getChunk() || event.getChunk() == bSpawn.getChunk())
			event.setCancelled(true);
		if (event.getChunk().equals(wSpawn.getChunk()) || event.getChunk().equals(bSpawn.getChunk()))
			event.setCancelled(true);
		
	}
	/*@EventHandler
	public void onNameTag(AsyncPlayerReceiveNameTagEvent event) {
		Player player = event.getNamedPlayer();
		String n = "";
		if (player.getName().length() > 15)
			n = player.getName().substring(15);
		else
			n = player.getName();
		event.setTag(PlayerMeta.getMeta(player).getTeam().color() + n);
	}*/

}
