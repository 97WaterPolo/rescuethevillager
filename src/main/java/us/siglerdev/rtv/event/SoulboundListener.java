package us.siglerdev.rtv.event;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import us.siglerdev.rtv.RescueTheVillager;

import java.util.Arrays;
import java.util.Iterator;

public class SoulboundListener implements Listener {

	private static final String soulboundTag = ChatColor.GOLD + "Soulbound";

	@EventHandler
	public void onSoulboundDrop(PlayerDropItemEvent event) {
		Player player = event.getPlayer();
		if(isSoulbound(event.getItemDrop().getItemStack())) {
			player.playSound(player.getLocation(), Sound.BLAZE_HIT, 1F, 1F);
			event.getItemDrop().remove();
		}
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		Iterator<ItemStack> it = event.getDrops().iterator();
		while(it.hasNext()) {
			if(isSoulbound(it.next())) {
				it.remove();
			}
		}
	}

	@EventHandler
	public void onClick(InventoryClickEvent event) {
		if(event.getView().getTopInventory().getType() != InventoryType.CRAFTING || event.getInventory().getType() != InventoryType.CRAFTING) {
			if(event.getCurrentItem() != null && event.getWhoClicked().getGameMode() != GameMode.CREATIVE) {
				if(isSoulbound(event.getCurrentItem())) {
					event.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void onPlayerCraft(CraftItemEvent event) {
		for(HumanEntity entity : event.getViewers()) {
			if(entity instanceof Player) {
				final Player player = (Player) entity;
				ItemStack[] item = event.getInventory().getMatrix();
				for(int i = 0; i < event.getInventory().getMatrix().length; i++) {
					if(item[i] != null) {
						if(item[i].hasItemMeta()) {
							if(SoulboundListener.isSoulbound(item[i])) {
								event.setCancelled(true);
								player.sendMessage(ChatColor.GRAY + "You can not use soulbound items to craft!");
								new BukkitRunnable() {
									@Override
									public void run() {
										player.closeInventory();
									}
								}.runTaskLater(RescueTheVillager.get(), 1L);
							}
						}
					}
				}
			}
		}
	}

	public static boolean isSoulbound(ItemStack item) {
		return item.hasItemMeta() && item.getItemMeta().hasLore() && item.getItemMeta().getLore().contains(soulboundTag);
	}

	public static void makeSoulbound(ItemStack item) {
		ItemMeta meta = item.getItemMeta();
		if(!meta.hasLore()) {
			meta.setLore(Arrays.asList(soulboundTag));
		} else {
			meta.getLore().add(soulboundTag);
		}
		item.setItemMeta(meta);
	}
	public static ItemStack makeSoulbound2(ItemStack item) {
		ItemMeta meta = item.getItemMeta();
		if(!meta.hasLore()) {
			meta.setLore(Arrays.asList(soulboundTag));
		} else {
			meta.getLore().add(soulboundTag);
		}
		item.setItemMeta(meta);
		return item;
	}
}