package us.siglerdev.rtv.event.player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.manager.team.GameTeam;
import us.siglerdev.rtv.resource.Config;
import us.siglerdev.rtv.util.Util;

public class PlayerDamage implements Listener {

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if (event.getEntity() instanceof Player){
			Player victim = (Player) event.getEntity();
			GameTeam v = PlayerMeta.getMeta(victim).getTeam();
			if (event.getDamager() instanceof Projectile){
				Projectile p = (Projectile) event.getDamager();
				if (p.getShooter() instanceof Player){
					Player shooter = (Player) p.getShooter();
					if (PlayerMeta.getMeta(shooter).getTeam() == v)
						event.setCancelled(true);
				}
			}
			if (event.getDamager() instanceof Player){
				Player dam = (Player) event.getDamager();
				if (PlayerMeta.getMeta(dam).getTeam() == v)
					event.setCancelled(true);
			}
		}
	}
	@EventHandler
	public void onDamage(EntityDamageEvent event){
		if (RescueTheVillager.get().endGame)
			event.setCancelled(true);
		if (event.getEntity() instanceof Player){
			Player p = (Player) event.getEntity();
			if (p.getWorld().getName().equalsIgnoreCase("lobby")){
				if (event.getCause() == DamageCause.VOID)
					p.teleport(Util.parseLocation(Bukkit.getWorld("lobby"), Config.get().getMap().getString("LobbySpawn")));
				event.setCancelled(true);
			}
		}
	}
}