package us.siglerdev.rtv.event.player;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.scheduler.BukkitRunnable;

import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.manager.team.GameTeam;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.manager.StatsManager;
import us.siglerdev.rtv.resource.Config;
import us.siglerdev.rtv.util.Util;

public class PlayerRespawn implements Listener {

	public static List<String> bypass = new ArrayList<String>();
	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {
		final Player player = event.getPlayer();
		PlayerMeta.getMeta(player).getKit().give(player);
		if (!bypass.contains(player.getName())){
			StatsManager statsManager = RescueTheVillager.get().getStats();
			statsManager.increaseDeaths(player);
			Util.updateGameScoreboard(player);

			if(player.getKiller() != null) {
				statsManager.increaseKills(player.getKiller());
				Util.updateGameScoreboard(player.getKiller());
			}
		}else
			bypass.remove(player.getName());
		new BukkitRunnable(){
			@Override
			public void run(){
				if (PlayerMeta.getMeta(player).getTeam() == GameTeam.WHITE)
					player.teleport(Util.parseLocation(Config.get().getMap().getString("World"), Config.get().getMap().getString("WhiteSpawn")));
				else
					player.teleport(Util.parseLocation(Config.get().getMap().getString("World"), Config.get().getMap().getString("BlueSpawn")));
			}
		}.runTaskLater(RescueTheVillager.get(), 1);

	}
}
