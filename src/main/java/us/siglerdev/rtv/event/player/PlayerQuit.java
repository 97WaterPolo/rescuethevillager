package us.siglerdev.rtv.event.player;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.manager.team.GameTeam;

public class PlayerQuit implements Listener {

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		event.setQuitMessage(null);
		if (PlayerMeta.getMeta(player).getTeam() != GameTeam.NONE){

		}
	}
}
