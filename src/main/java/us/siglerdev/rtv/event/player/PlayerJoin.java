package us.siglerdev.rtv.event.player;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.event.SoulboundListener;
import us.siglerdev.rtv.manager.Locale;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.manager.team.GameTeam;
import us.siglerdev.rtv.resource.Config;
import us.siglerdev.rtv.resource.GameItems;
import us.siglerdev.rtv.resource.bar.BarUtil;
import us.siglerdev.rtv.util.Util;

public class PlayerJoin implements Listener {

	/**
	 * Handles the accepted join of a player. Sets scoreboards here based on the game state.
	 */
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		final Player player = event.getPlayer();
		for(int x = 0; x < 150; x++) {
			player.sendMessage(" ");
		}
		new BukkitRunnable() {
			int counter = 0;
			@Override
			public void run() {
				if(!player.isOnline()) {
					this.cancel();
				}
				String[] s = new String[] {"1", "2", "3", "4", "5", "6", "9", "a", "b", "c", "d", "e"};
				if(counter >= s.length) {
					counter = 0;
				}
				BarUtil.setMessageAndPercent(player, ChatColor.translateAlternateColorCodes('&', "  &" + s[counter] + " &lRescue The Villager &f&l| MC.MINEORIGINS.NET"), 1F);
				counter++;
			}
		}.runTaskTimer(RescueTheVillager.get(), 20, 20);
		Util.resetPlayer(player);
		player.teleport(Util.parseLocation("lobby", Config.get().getMap().getString("LobbySpawn")));
		event.setJoinMessage(Locale.JOIN.msg().replace("%player%", player.getName()) + " " + ChatColor.translateAlternateColorCodes('&',
				"&ejoined the game &7(&b" + Bukkit.getOnlinePlayers().size()+"&7/&b"+ Config.get().getConfig().getInt("General.MinPlayers") +"&7)"));

		player.sendMessage(Locale.JOIN_MESSAGE.msg(player));
		if (!RescueTheVillager.get().gameStarted){ //The game is in lobby state.
			Util.checkStarting(false);
		}else{
			if (PlayerMeta.getMeta(player).getTeam() == GameTeam.NONE){
				PlayerMeta.getMeta(player).setTeam(Util.getSmalletTeam());
				player.setHealth(0);
				Util.updateGameScoreboard(player);
				RescueTheVillager.get().getSiglerBoard().updateTeams(player);
			}else{
				player.setHealth(0);
				Util.updateGameScoreboard(player);
			}
				
		}
	}

	/**
	 * Handles the joining of premium player's. Override has a max limit, admin can join game's any size.
	 */
	@EventHandler
	public void onPreLogin(AsyncPlayerPreLoginEvent event) {
		Player player = Bukkit.getPlayer(event.getUniqueId());
		if(Bukkit.getOnlinePlayers().size() >= Config.get().getConfig().getInt("General.MaxPlayers")) {
			if(!player.hasPermission("rtv.join.admin") && !player.hasPermission("rtv.join.override")) {
				event.setKickMessage(Locale.FULL_MATCH.msg(player));
				event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_FULL, Locale.FULL_MATCH.msg(player));
			} else if(player.hasPermission("rtv.join.override") && Bukkit.getOnlinePlayers().size() >= Config.get().getConfig().getInt("General.AbsPlayers")) {
				event.setKickMessage(Locale.FULL_MATCH_PREM.msg(player));
				event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_FULL, Locale.FULL_MATCH_PREM.msg(player));
			}
		}
	}
}