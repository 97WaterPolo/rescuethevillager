package us.siglerdev.rtv.event.player;

import net.minecraft.server.v1_7_R4.EntityPlayer;
import net.minecraft.server.v1_7_R4.EnumClientCommand;
import net.minecraft.server.v1_7_R4.PacketPlayInClientCommand;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.scheduler.BukkitRunnable;

import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.manager.StatsManager;
import us.siglerdev.rtv.resource.Config;
import us.siglerdev.rtv.util.Util;

public class PlayerDeath implements Listener {

	int x = 0;
	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		x++;
		if (x >= 10){
			Bukkit.getWorld(Config.get().getMap().getString("World")).setTime(0);
			x=0;
		}
		final Player player = event.getEntity();
		if (!PlayerRespawn.bypass.contains(player.getName())){
			if (player.getKiller() != null)
				event.setDeathMessage(ChatColor.translateAlternateColorCodes('&', PlayerMeta.getMeta(player).getTeam().color() + player.getName() + " &7was killed by "+ 
						PlayerMeta.getMeta(player.getKiller()).getTeam().color() + player.getKiller().getName()));
			else
				event.setDeathMessage(ChatColor.translateAlternateColorCodes('&', PlayerMeta.getMeta(player).getTeam().color() + player.getName() + " &7was killed!"));
		}
		new BukkitRunnable() {
			@Override
			public void run() {
				PacketPlayInClientCommand in = new PacketPlayInClientCommand(EnumClientCommand.PERFORM_RESPAWN);
				EntityPlayer ep = ((CraftPlayer) player).getHandle();
				ep.playerConnection.a(in);
			}
		}.runTaskLater(RescueTheVillager.get(), 1L);
	}
}
