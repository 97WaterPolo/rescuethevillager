package us.siglerdev.rtv.event.player;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import us.siglerdev.rtv.manager.PlayerMeta;

public class PlayerChat implements Listener
{
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onChat(AsyncPlayerChatEvent event){
		Player player = event.getPlayer();
		if (player.getWorld().getName().equalsIgnoreCase("lobby")){
			event.setCancelled(true);
			player.sendMessage(ChatColor.DARK_RED + "Chat is disabled before the game starts!");
		}else{
			Bukkit.broadcastMessage(PlayerMeta.getMeta(player).getTeam().color() + player.getName() + ChatColor.RED +": " + ChatColor.WHITE + event.getMessage());
		}
		event.setCancelled(true);
	}

}
