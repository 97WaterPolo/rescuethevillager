package us.siglerdev.rtv.event.player;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import us.siglerdev.rtv.RescueTheVillager;
import us.siglerdev.rtv.manager.PlayerMeta;
import us.siglerdev.rtv.resource.GameItems;
import us.siglerdev.rtv.resource.ParticleEffect;
import us.siglerdev.rtv.resource.ParticleEffect.ParticleType;
import us.siglerdev.rtv.util.Util;

public class PlayerInteract implements Listener
{
	public static List<BlockState> ironBS = new ArrayList<BlockState>();
	@EventHandler
	public void onVillagerRelease(PlayerInteractEvent event){
		ItemStack is = event.getItem();
		Player player = event.getPlayer();
		Block b = event.getClickedBlock();
		if (is == null || event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_AIR)
			return;
		if (b == null)
			return;
		if (b.getType() != Material.IRON_FENCE)
			return;
		if (is.getType() == GameItems.MAGIC_KEY.getType()){
			if (is.getItemMeta().getDisplayName().contains(GameItems.MAGIC_KEY.getItemMeta().getDisplayName())){
				Location l = b.getLocation().clone();
				Location l2 = b.getLocation().clone().add(0,1,0);
				Location l3 = b.getLocation().clone().add(0,-1,0);
				ParticleEffect s = new ParticleEffect(ParticleType.CLOUD, 10, 10, .25);
				if (b.getWorld().getBlockAt(l).getType() == Material.IRON_FENCE){
					ironBS.add(b.getWorld().getBlockAt(l).getState());
					b.getWorld().getBlockAt(l).setType(Material.AIR);
					s.sendToLocation(b.getWorld().getBlockAt(l).getLocation());
				}
				if (b.getWorld().getBlockAt(l2).getType() == Material.IRON_FENCE){
					ironBS.add(b.getWorld().getBlockAt(l2).getState());
					b.getWorld().getBlockAt(l2).setType(Material.AIR);
					s.sendToLocation(b.getWorld().getBlockAt(l2).getLocation());
				}
				if (b.getWorld().getBlockAt(l3).getType() == Material.IRON_FENCE){
					ironBS.add(b.getWorld().getBlockAt(l3).getState());
					b.getWorld().getBlockAt(l3).setType(Material.AIR);
					s.sendToLocation(b.getWorld().getBlockAt(l3).getLocation());
				}
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', PlayerMeta.getMeta(player).getTeam().color() +player.getName()+ " &6has released the &aVillager!"));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6+25 Coins!"));
				RescueTheVillager.get().rescuer = player.getName();
				for (Player team : PlayerMeta.getMeta(player).getTeam().getPlayers())
					if (team.getName() != player.getName())
					team.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6+8 Coins!"));
				for (int x = 0; x < 13; x++){
					Util.spawnFirework(player.getLocation());
				}
				RescueTheVillager.get().villagerReleased = true;
				player.setItemInHand(null);
			}
		}

	}
	@EventHandler
	public void onInteract(PlayerInteractEvent event){
		Player player = event.getPlayer();
		ItemStack is = event.getItem();
		if (is == null || event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK)
			return;
		if (is.getType() == GameItems.TEAM_CHOOSER.getType() && is.getItemMeta().getDisplayName().contains("Team")){
			Inventory i = Bukkit.createInventory(null, 9*6, "Select Team");
			i.setItem(21,GameItems.TEAM_CHOOSER_BLUE);
			i.setItem(23,GameItems.TEAM_CHOOSER_WHITE);
			player.openInventory(i);
		}
		if (is.getType() == GameItems.KIT_SELECTOR.getType() && is.getItemMeta().getDisplayName().contains("Kit")){
			Util.showClassSelector(player, "Select Kit");
		}
	}

}
